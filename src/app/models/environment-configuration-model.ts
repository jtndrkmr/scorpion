import { Injectable } from '@angular/core';
@Injectable()

export class EnvironmentConfigurationModel{

    domainURL = "http://demo.imitab.com/";
    //webServiceURL = "http://demo.imitab.com/web_services/";
    webServiceURL = "https://scorpionhr.com/AngularServices/web_services/";

    getDomainURL():any{
        return this.domainURL;
    }

    getDomainServiceURL():any{
        return this.webServiceURL;
    }
}