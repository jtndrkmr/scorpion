import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppDataService } from './../_services/user-data-service';
import { AuthenticationService } from './../_services/authentication.service';

import { LoginRouterModule } from './login-routing.config'
import { LoginSectionComponent } from './login-section/login-section.component';
import { ResetPasswordSectionComponent } from './reset-password-section/reset-password-section.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LoginRouterModule
  ],
  declarations: [
    LoginSectionComponent,
    ResetPasswordSectionComponent
  ],
  providers:[
    AppDataService,
    AuthenticationService
  ]
})
export class LoginModule { }
