import { Component, OnInit } from '@angular/core';
import { LoginModal } from '../../../common/modals/userModal';
import { Router,NavigationEnd,ActivatedRoute } from '@angular/router';
import { AppDataService } from './../../_services/user-data-service';
import { AuthenticationService } from './../../_services/authentication.service';
import { SharedService } from './../../_services/shared-service';

@Component({
  selector: 'app-login-section',
  templateUrl: './login-section.component.html',
  styleUrls: ['./login-section.component.css']
})
export class LoginSectionComponent implements OnInit {

  loginInfo: LoginModal = new LoginModal();
  loginFail:boolean = false;
  loginFailTxt:string = "Login Credential Failed!";

  constructor(
    private router: Router,
    private appDataService: AppDataService,
    private authenticationService: AuthenticationService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    
  }

  forgotPassword(){
    this.router.navigate(["login/reset"]);
  }

  onChangeVal(){
    this.loginFail = false;
  }

  signInAction(){
    if(this.loginInfo.email && this.loginInfo.password){
      var authenticationObj = {
        "txt_UserID":this.loginInfo.email,
        "txt_Password":this.loginInfo.password
      };
      
      this.authenticationService.login(authenticationObj).subscribe(response=>{
        console.log("response -->",response);
        let eCodeVal = (response[0] && response != []) ? response[0].E_CODE : "";
        if(response[0].isLoggedIn && response[0].isLoggedIn != "false"){
          this.loginFail = true;
          console.log("Login Failed!");
        }
        else if(eCodeVal != "" && !response[0].isLoggedIn){
          //window.location.href = "http://demo.imitab.com/adminty-masterbkp/default/Dash2.htm";
          /*let sidbarObj = {
            "txt_Branch":response[0].Branch,
            "txt_UserID":response[0].E_CODE
          };*/
          this.sharedService.setLoggedUserObject(response[0]);
          localStorage.setItem('currentUser', JSON.stringify(response[0]));
          this.router.navigate(["dashboard"]);
        }else{
          this.loginFail = true;
          console.log("Login Failed!");
        }
      })
      console.log("email ->"+this.loginInfo.email+" Password ->"+this.loginInfo.password);
    } 
  }



}
