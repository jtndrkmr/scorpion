import { Component, OnInit } from '@angular/core';
import { Router,NavigationEnd,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password-section',
  templateUrl: './reset-password-section.component.html',
  styleUrls: ['./reset-password-section.component.css']
})
export class ResetPasswordSectionComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  loginPage(){
    this.router.navigate(["login"]);
  }

}
