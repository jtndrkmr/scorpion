import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { LoginSectionComponent } from './login-section/login-section.component';
import { ResetPasswordSectionComponent } from './reset-password-section/reset-password-section.component';

const routes: Routes = [
    {path: '', component:LoginSectionComponent},
    {path: 'reset', component:ResetPasswordSectionComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class LoginRouterModule{
}
