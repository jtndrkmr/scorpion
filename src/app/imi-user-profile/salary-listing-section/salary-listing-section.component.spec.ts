import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaryListingSectionComponent } from './salary-listing-section.component';

describe('SalaryListingSectionComponent', () => {
  let component: SalaryListingSectionComponent;
  let fixture: ComponentFixture<SalaryListingSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalaryListingSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaryListingSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
