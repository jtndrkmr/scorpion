import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from './../../_services/shared-service';
import { AppDataService } from './../../_services/user-data-service';
import { DataListingService } from './../../_services/data-listing-service';

@Component({
  selector: 'imi-salary-listing-section',
  templateUrl: './salary-listing-section.component.html',
  styleUrls: ['./salary-listing-section.component.css']
})
export class SalaryListingSectionComponent implements OnInit {

  genders = [];
  cols = [];
  degination = [];
  yearFilter: number;
  yearTimeout: any;

  private loggedUserObj:any;
  public role:any;
  private userObj:any;
  usersList:any;

  constructor(
    private router: Router,
    private appDataService: AppDataService,
    private dataListingService: DataListingService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
    this.role = this.loggedUserObj.ROLE;
    this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
    var designObj = {"txt_Branch":this.loggedUserObj.Branch,"txt_Type":"EMP_DESIG"};
    this.sharedService.setEditableFormFlag(false);
    this.designationList(designObj);
    this.getUsersList(this.userObj);
  }

  designationList(designObj){
    this.dataListingService.getDeginationList(designObj).subscribe(response=>{
      this.degination = response;
    });
  }

  getUsersList(userObj){
    this.appDataService.getUsersListingByRole(userObj).subscribe(response=>{
      this.usersList = response;
      this.getTableColum(this.usersList);
    });
  }

  getTableColum(users){
    this.cols = [
      { field: 'E_CODE', header: 'E CODE', display: 'table-cell' },
      { field: 'EmpName', header: 'Name', display: 'table-cell' },
      { field: 'Gender', header: 'Gender', display: 'table-cell' },
      { field: 'DOJ', header: 'Date Of Joining', display: 'table-cell' },
      { field: 'Designation', header: 'Designation', display: 'table-cell' },
      { field: 'E_CODE', header: 'Action', display: 'table-cell' },
      { field: 'Branch', header: 'Branch', display: 'none'}
    ];

    this.genders = [
      { label: 'All', value: null },
      { label: 'Male', value: 'M' },
      { label: 'Female', value: 'F' }
    ];

    /*var dummArr:any = [];
    users.forEach(element => {
      dummArr.push(element.Designation);
    });
    var unique = dummArr.filter((v, i, a) => a.indexOf(v) === i);
    dummArr = [];
    unique.forEach(element => {
      dummArr.push({label:element, value:element});
    });
    this.degination = dummArr;*/
  }

  onYearChange(event, dt) {
    if (this.yearTimeout) {
        clearTimeout(this.yearTimeout);
    }

    this.yearTimeout = setTimeout(() => {
        dt.filter(event.value, 'year', 'gt');
    }, 250);
  }

  editSalaryDetailsById(EU_CODE, EU_BRANCH){
    var editUserObj = {"EU_BRANCH":EU_BRANCH,"EU_CODE":EU_CODE};    
    this.sharedService.setSalaryMasterFormFlag(editUserObj);
    this.router.navigate(["dashboard/salary"]);
  }

}
