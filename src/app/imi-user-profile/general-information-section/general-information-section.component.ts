import { Component, Renderer2, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppDataService } from './../../_services/user-data-service';
import { DataListingService } from './../../_services/data-listing-service';
import { SharedService } from './../../_services/shared-service';
import { RequestPost } from './../../models/imi-request-post-model';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'imi-general-information-section',
  templateUrl: './general-information-section.component.html',
  styleUrls: ['./general-information-section.component.css']
})
export class GeneralInformationSectionComponent implements OnInit {

  @Input('editUserData') editUserData: any;
  fromlistingPage: any;
  generalInformationForm: FormGroup;
  requestPost = new RequestPost();
  private generalInfoData: any;
  private userObj: any;
  public createNewFlag: boolean = false;
  hrEditable: boolean = false;
  private loggedUserObj: any;
  public viewFieldDetailArr: any;
  public viewFields: any;
  public role: any;
  public requiredFields: any;
  ecode_val: string = "";
  username_val: string = "";
  joiningdate_val: any;
  profileimg_val: any;

  e_type_val: any;

  bloodGroupList: any;
  castList: any;
  basisofList: any;
  categoryList: any;
  designationList: any;
  entityList: any;
  empTypeList: any;
  functionList: any;
  gradeList: any;
  joiningUnitList: any;
  maritialStatusList: any;
  matrixList: any;
  noticeTypeList: any;
  religinList: any;
  reportingList: any;
  roleList: any;
  subFunctionList: any;
  staticMsgs: any;
  resstatus: any;
  minDate: string;
  maxDate: any;
  eCodeVal: any;

  imageSelectedString: any;
  imageSelectedName: any;
  join_min: any;

  constructor(
    private _fb: FormBuilder,
    private appDataService: AppDataService,
    private dataListingService: DataListingService,
    private sharedService: SharedService,
    private sanitizer: DomSanitizer,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private renderer: Renderer2,
  ) {

  }

  ngOnInit() {
    this.prepareReactForm();
    this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
    //this.convertingImage(this.loggedUserObj.Column1);
    this.role = this.loggedUserObj.ROLE;
    this.userObj = { "txt_Branch": this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE };
    let menuGen = { "txt_Menu_Type": "gen" };
    //console.log("Input/Output Data-->",this.editUserData)
    this.StaticListRecord(this.loggedUserObj.Branch);
    this.fromlistingPage = this.sharedService.editableFlag.value;
    //edit user general info by Admin/HR
    if (this.editUserData == 'New')
      this.createNewFlag = true;

    if (this.editUserData == 'New' && this.fromlistingPage) {
      this.hrEditable = true;
      this.eCodeVal = "";
      this.blankSection();
      this.generalInformationValue({ "txt_Branch": this.loggedUserObj.Branch, "txt_UserID": "" }, menuGen, this.hrEditable);

    }
    else if (this.editUserData != undefined && this.editUserData != 'New' && this.fromlistingPage) {
      this.hrEditable = true;
      this.eCodeVal = this.editUserData.EU_CODE;
      this.generalInformationValue({ "txt_Branch": this.editUserData.EU_BRANCH, "txt_UserID": this.editUserData.EU_CODE }, menuGen, this.hrEditable);

      this.userBasicDetail({ "txt_Branch": this.editUserData.EU_BRANCH, "txt_UserID": this.editUserData.EU_CODE });


    }
    else {
      this.eCodeVal = this.userObj.txt_UserID;
      this.generalInformationValue(this.userObj, menuGen, this.hrEditable);


      this.userBasicDetail(this.userObj);

    }


    this.staticMsgs = this.sharedService.getStaticMessages();



    setTimeout(() => {
      //this.sharedService.setEditableFormFlag(false);
    }, 5000);
  }

  // Getting All Dropdown Values using in General Information
  StaticListRecord(branch) {
    var reqObj: any = { "txt_Branch": branch, "txt_Type": "GI_ALL" };
    this.dataListingService.getStaticValuesList(reqObj).subscribe(response => {
      var listRecord = response;

      this.castList = listRecord.GI_CASTE;
      this.bloodGroupList = listRecord.GI_BLOOD;
      this.basisofList = listRecord.GI_BOFF;
      this.categoryList = listRecord.GI_CAT;
      this.designationList = listRecord.GI_DESIG;
      this.entityList = listRecord.GI_ENT;
      this.empTypeList = listRecord.GI_ETYPE;
      this.functionList = listRecord.GI_FUNC;
      this.gradeList = listRecord.GI_GRADE;
      this.joiningUnitList = listRecord.GI_JU;
      this.maritialStatusList = listRecord.GI_MAR_S;
      this.matrixList = listRecord.GI_REPO;
      this.noticeTypeList = listRecord.GI_NTYPE;
      this.religinList = listRecord.GI_REL;
      this.reportingList = listRecord.GI_REPO;
      this.resstatus = listRecord.GI_RES;
      this.roleList = listRecord.GI_ROLE;
      this.subFunctionList = listRecord.GI_SUB_FUNC;

    });
  }

  // Setting values blank on create new employee
  blankSection() {
    this.ecode_val = "";
    this.username_val = "";
    this.joiningdate_val = "";
    this.profileimg_val = "../../../assets/images/user-profile/user-dummy.png";
  }

  prepareReactForm() {
    this.generalInformationForm = this._fb.group({
      E_CODE: [''],
      AUTO_E_CODE: [''],
      NAME_1ST: [''],
      NAME_2ND: [''],
      NAME_SUR: [''],
      BIRTH_DT: [''],
      JOIN_DT: [''],
      Join_age_ro: [''],
      Retire_age_ro: [''],
      RETIREMENT_DATE: [''],
      Curr_age_ro: [''],
      SEX: [''],
      HEIGHT: [''],
      WEIGHT: [''],
      JOIN_UNIT: [''],
      Function_Code: [''],
      Cat_Code: [''],
      Sub_Function_Code: [''],
      GRADE: [''],
      DESIGN: [''],
      RES_ST: [''],
      Cast_Cd: [''],
      MAR_ST: [''],
      RELI_CD: [''],
      CLIENT_CODE: [''],
      BL_GR: [''],
      HOD_CODE: [''],
      MM_E_Code: [''],
      E_TYPE: [''],
      RM_E_Code: [''],
      TRA_FROM: [''],
      TRA_TO: [''],
      TRA_DAYS: [''],
      SAN_FROM: [''],
      SAN_TO: [''],
      SAN_DAYS: [''],
      PROB_FROM: [''],
      PROB_TO: [''],
      PROB_DAYS: [''],
      CONFIRM_ON: [''],
      Notice_Period_Type: [''],
      Notice_Period: [''],
      Calculation_Basis: [''],
      Joinning_Bonus: [''],
      E_MAIL_ID: [''],
      METRO_NONMETRO: [''],
      ALT_E_MAIL_ID: [''],
      PersonalIDMark: [''],
      ROLE_CODE: ['']
    });

    this.fieldRequiredControls()
  }

  patchFormValues(generalInfoData) {
    this.generalInformationForm.patchValue({
      E_CODE: this.eCodeVal,
      AUTO_E_CODE: generalInfoData.AUTO_E_CODE.VAL,
      NAME_1ST: generalInfoData.NAME_1ST.VAL,
      NAME_2ND: generalInfoData.NAME_2ND.VAL,
      NAME_SUR: generalInfoData.NAME_SUR.VAL,
      BIRTH_DT: (generalInfoData.BIRTH_DT.VAL) ? moment(generalInfoData.BIRTH_DT.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : '',
      JOIN_DT: (generalInfoData.JOIN_DT.VAL) ? moment(generalInfoData.JOIN_DT.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : "",
      Join_age_ro: generalInfoData.Join_age_ro.VAL,
      Retire_age_ro: generalInfoData.Retire_age_ro.VAL,
      RETIREMENT_DATE: (generalInfoData.RETIREMENT_DATE.VAL) ? moment(generalInfoData.RETIREMENT_DATE.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : "",
      Curr_age_ro: generalInfoData.Curr_age_ro.VAL,
      SEX: generalInfoData.SEX.VAL,
      HEIGHT: generalInfoData.HEIGHT.VAL,
      WEIGHT: generalInfoData.WEIGHT.VAL,
      JOIN_UNIT: generalInfoData.JOIN_UNIT.VAL,
      Function_Code: generalInfoData.Function_Code.VAL,
      Cat_Code: generalInfoData.Cat_Code.VAL,
      Sub_Function_Code: generalInfoData.Sub_Function_Code.VAL,
      GRADE: generalInfoData.GRADE.VAL,
      DESIGN: generalInfoData.DESIGN.VAL,
      RES_ST: generalInfoData.RES_ST.VAL,
      Cast_Cd: generalInfoData.Cast_Cd.VAL,
      MAR_ST: generalInfoData.MAR_ST.VAL,
      RELI_CD: generalInfoData.RELI_CD.VAL,
      CLIENT_CODE: generalInfoData.CLIENT_CODE.VAL,
      BL_GR: generalInfoData.BL_GR.VAL,
      HOD_CODE: generalInfoData.HOD_CODE.VAL,
      MM_E_Code: generalInfoData.MM_E_Code.VAL,
      E_TYPE: generalInfoData.E_TYPE.VAL,
      RM_E_Code: generalInfoData.RM_E_Code.VAL,
      TRA_FROM: (generalInfoData.TRA_FROM.VAL) ? moment(generalInfoData.TRA_FROM.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : "",
      TRA_TO: (generalInfoData.TRA_TO.VAL) ? moment(generalInfoData.TRA_TO.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : "",
      SAN_FROM: (generalInfoData.SAN_FROM.VAL) ? moment(generalInfoData.SAN_FROM.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : "",
      SAN_TO: (generalInfoData.SAN_TO.VAL) ? moment(generalInfoData.SAN_TO.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : "",
      PROB_FROM: (generalInfoData.PROB_FROM.VAL) ? moment(generalInfoData.PROB_FROM.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : "",
      PROB_TO: (generalInfoData.PROB_TO.VAL) ? moment(generalInfoData.PROB_TO.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : "",
      CONFIRM_ON: (generalInfoData.CONFIRM_ON.VAL) ? moment(generalInfoData.CONFIRM_ON.VAL, 'MMM DD YYYY hh:mmA').format('DD/MM/YYYY') : "",
      Notice_Period_Type: generalInfoData.Notice_Period_Type.VAL,
      Notice_Period: generalInfoData.Notice_Period.VAL,
      Calculation_Basis: generalInfoData.Calculation_Basis.VAL,
      Joinning_Bonus: generalInfoData.Joinning_Bonus.VAL,
      E_MAIL_ID: generalInfoData.E_MAIL_ID.VAL,
      METRO_NONMETRO: generalInfoData.METRO_NONMETRO.VAL,
      ALT_E_MAIL_ID: generalInfoData.ALT_E_MAIL_ID.VAL,
      PersonalIDMark: generalInfoData.PersonalIDMark.VAL,
      ROLE_CODE: generalInfoData.ROLE_CODE.VAL,
      SAN_DAYS: moment.duration(moment(generalInfoData.SAN_FROM.VAL, "MMM DD YYYY hh:mmA").diff(moment(generalInfoData.SAN_TO.VAL, "MMM DD YYYY hh:mmA"))).asDays(),
      TRA_DAYS: moment.duration(moment(generalInfoData.TRA_FROM.VAL, "MMM DD YYYY hh:mmA").diff(moment(generalInfoData.TRA_TO.VAL, "MMM DD YYYY hh:mmA"))).asDays(),
      PROB_DAYS: moment.duration(moment(generalInfoData.PROB_FROM.VAL, "MMM DD YYYY hh:mmA").diff(moment(generalInfoData.PROB_TO.VAL, "MMM DD YYYY hh:mmA"))).asDays()
    });
  }

  generalInformationValue(userObj, menuGen, hrEditable) {
    const genInfoObj = Object.assign(userObj, menuGen);
    this.appDataService.getGeneralInformationDetails(genInfoObj).subscribe(response => {

      this.generalInfoData = response;

      this.patchFormValues(this.generalInfoData);

      this.viewFieldDetailArr = this.filterFieldsByRole(this.generalInfoData, this.loggedUserObj.ROLE, hrEditable);

      this.viewFields = this.filterViewFields(this.viewFieldDetailArr);

      this.requiredFields = this.filterRequiredFields(this.viewFieldDetailArr, this.loggedUserObj.ROLE, hrEditable);


      this.appDataService.getLatestECODEValue({ "txt_Branch": this.loggedUserObj.Branch }).subscribe(data => {
        this.generalInformationForm.patchValue({
          E_CODE: data[0].L_E_CODE
        });
        this.generalInformationForm.controls['E_CODE'].disable();
        this.generalInformationForm.controls['AUTO_E_CODE'].setValue(true);
        this.generalInformationForm.controls['AUTO_E_CODE'].disable();
      });

    });
  }

  userBasicDetail(userObj) {
    this.appDataService.getUserBasicDetails(userObj).subscribe(response => {
      if (response.length > 0) {
        var userBasicDetail = response[0];
        this.ecode_val = userBasicDetail.E_CODE;
        this.username_val = userBasicDetail.FullName;
        this.joiningdate_val = userBasicDetail.J_DATE;
        this.convertingImage(userBasicDetail.IMG, userBasicDetail.IMAGE_TYPE);
      }
    });
  }

  convertingImage(blobImg, IMAGE_TYPE) {
    let objectURL = 'data:image/' + IMAGE_TYPE + ';base64,' + blobImg;
    this.profileimg_val = objectURL;
  }

  filterFieldsByRole(generalData: any, role: string, hrEditable: boolean) {
    var fieldsArray: any = [];
    if (hrEditable == true) {
      for (var key in generalData) {
        if (generalData[key].HV == 'Y') { fieldsArray.push(generalData[key]); generalData[key]["field"] = key; }
      }
    } else {
      for (var key in generalData) {
        if (generalData[key].UV == 'Y') { fieldsArray.push(generalData[key]); generalData[key]["field"] = key; }
      }
    }
    return fieldsArray;
  }

  filterViewFields(viewFieldDetailArr: any) {
    let fieldsArray: any = [];
    viewFieldDetailArr.forEach(element => {
      fieldsArray.push(element.field);
    });
    return fieldsArray;
  }


  filterRequiredFields(viewFieldDetailArr: any, role: any, hrEditable: boolean) {

    let requiredField: any = [];
    var keys: any = this.generalInformationForm.getRawValue();

    if (hrEditable == true) {
      viewFieldDetailArr.forEach(element => {
        const control = this.generalInformationForm.get(element.field);
        if (control != null) {
          if (element.HE == 'Y') {
            if (element.RO == 'Y') {
              requiredField.push(element.field);
              this.generalInformationForm.controls[element.field].setValidators([Validators.required]);
            }
          }
          else {
            this.generalInformationForm.controls[element.field].disable();
            if (element.field == "PROB_FROM") {
              this.generalInformationForm.controls["PROB_DAYS"].disable();
            }
            if (element.field == "SAN_FROM") {
              this.generalInformationForm.controls["SAN_DAYS"].disable();
            }
            if (element.field == "TRA_FROM") {
              this.generalInformationForm.controls["TRA_DAYS"].disable();
            }
          }
        }
      });
    }
    else {

      viewFieldDetailArr.forEach(element => {
        const control = this.generalInformationForm.get(element.field);
        if (control != null) {
          if (element.UE == 'Y') {
            if (element.RO == 'Y') {
              requiredField.push(element.field);
              this.generalInformationForm.controls[element.field].setValidators([Validators.required]);
            }
          }
          else {
            this.generalInformationForm.controls[element.field].disable();
            if (element.field == "PROB_FROM") {
              this.generalInformationForm.controls["PROB_DAYS"].disable();
            }
            if (element.field == "SAN_FROM") {
              this.generalInformationForm.controls["SAN_DAYS"].disable();
            }
            if (element.field == "TRA_FROM") {
              this.generalInformationForm.controls["TRA_DAYS"].disable();
            }
          }
        }
      });



    }

    requiredField.push("Joinning_Bonus");
    this.generalInformationForm.controls["Joinning_Bonus"].setValidators([Validators.pattern("^[0-9]*$")]);
    requiredField.push("Notice_Period");
    this.generalInformationForm.controls["Notice_Period"].setValidators([Validators.pattern("^[0-9]*$")]);

    return requiredField;

  };



  filterRequiredFields_old(viewFieldDetailArr: any, role: any, hrEditable: boolean) {

    let requiredField: any = [];
    var keys: any = this.generalInformationForm.getRawValue();

    if (hrEditable == true) {
      viewFieldDetailArr.forEach(element => {


        if (element.RO == 'Y') {
          requiredField.push(element.field);
        }
        if (element.HE == 'Y') {

          for (let key in keys) {
            if (key == element.field) {
              this.generalInformationForm.controls[element.field].setValidators([Validators.required]);
            }
          }
        } else {
          for (let key in keys) {
            if (key == element.field)
              this.generalInformationForm.controls[element.field].disable();
            if (element.field == "PROB_FROM") {
              this.generalInformationForm.controls["PROB_DAYS"].disable();
            }
            if (element.field == "SAN_FROM") {
              this.generalInformationForm.controls["SAN_DAYS"].disable();
            }
            if (element.field == "TRA_FROM") {
              this.generalInformationForm.controls["TRA_DAYS"].disable();
            }
          }
        }
      });
    } else {
      viewFieldDetailArr.forEach(element => {
        if (element.RO == 'Y') {
          requiredField.push(element.field);
        }
        if (element.UE == 'Y') {
          //requiredField.push(element.field); 
          for (let key in keys) {
            if (key == element.field)
              this.generalInformationForm.controls[element.field].setValidators([Validators.required]);
          }
        } else {
          //console.log("element.field tada-->",element.field);
          for (let key in keys) {
            if (key == element.field)
              this.generalInformationForm.controls[element.field].disable();
            if (element.field == "PROB_FROM") {
              this.generalInformationForm.controls["PROB_DAYS"].disable();
            }
            if (element.field == "SAN_FROM") {
              this.generalInformationForm.controls["SAN_DAYS"].disable();
            }
            if (element.field == "TRA_FROM") {
              this.generalInformationForm.controls["TRA_DAYS"].disable();
            }
          }

        }
      });
    }


    return requiredField;
  }

  J_AGE_CALC() {
    //console.log(this.t1);
    // console.log(this.t1);
    // console.log(this.t1);
    //.format("DD/MM/YYYY")
    //console.log(this.generalInformationForm.controls['BIRTH_DT'].value)
    //console.log(this.generalInformationForm.controls['JOIN_DT'].value)
    if (this.generalInformationForm.controls['BIRTH_DT'].value != "" && this.generalInformationForm.controls['JOIN_DT'].value) {
      var t1 = moment(this.generalInformationForm.controls['BIRTH_DT'].value, "DD/MM/YYYY");
      var t2 = moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY");
      this.generalInformationForm.controls['Join_age_ro'].setValue(t2.diff(t1, 'days'));
    }

    if (this.generalInformationForm.controls['BIRTH_DT'].value != "" && this.generalInformationForm.controls['JOIN_DT'].value) {

      var t2 = moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY");
      var t3 = moment(this.generalInformationForm.controls['RETIREMENT_DATE'].value, "DD/MM/YYYY");
      this.generalInformationForm.controls['Retire_age_ro'].setValue(t3.diff(t2, 'days'));
    }
    this.E_TYPE_F();


    //console.log(t1);
    //console.log(t2);



    //this.join_min=new Date();

    //this.join_min=moment("22/01/2020", "DD/MM/YYYY");
    //var x=moment(this.t1,"MMM DD YYYY hh:mmA");
    //var y=moment(this.t2,"MMM DD YYYY hh:mmA");
    //console.log(t1);
    //console.log(t2);
    //console.log(t2.diff(t1, 'days'));

  }





  E_TYPE_F() {
    this.e_type_val = this.generalInformationForm.controls['E_TYPE'].value;
    console.log(this.e_type_val);

    if (this.e_type_val != "") {
      // var date=moment(this.generalInformationForm.controls['TRA_FROM'].value, "DD/MM/YYYY").add(input_val, 'days').format('DD/MM/YYYY');


      //var date_dump=moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY");

      if (this.e_type_val == '01') {
        console.log("Permanent-01");

        var date_dump = (this.generalInformationForm.controls['JOIN_DT'].value != "") ? moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY").add(0, 'days').format('DD/MM/YYYY') : "";
        //console.log(date);
        this.generalInformationForm.controls['CONFIRM_ON'].setValue(date_dump);
        this.generalInformationForm.controls['CONFIRM_ON'].disable();


      }

      else if (this.e_type_val == '03') {
        console.log("TRAINEE-03");
        //var date=moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY");
        //console.log(date);

        var date_dump = (this.generalInformationForm.controls['JOIN_DT'].value != "") ? moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY").add(0, 'days').format('DD/MM/YYYY') : "";

        this.generalInformationForm.controls['TRA_FROM'].setValue(date_dump);
        this.generalInformationForm.controls['TRA_FROM'].disable();


        var date2 = (this.generalInformationForm.controls['TRA_TO'].value != "") ? moment(this.generalInformationForm.controls['TRA_TO'].value, "DD/MM/YYYY").add(1, 'days').format('DD/MM/YYYY') : "";

        this.generalInformationForm.controls['PROB_FROM'].setValue(date2);
        this.generalInformationForm.controls['PROB_FROM'].disable();

        var date3 = (this.generalInformationForm.controls['PROB_TO'].value != "") ? moment(this.generalInformationForm.controls['PROB_TO'].value, "DD/MM/YYYY").add(1, 'days').format('DD/MM/YYYY') : "";

        this.generalInformationForm.controls['CONFIRM_ON'].setValue(date3);
        this.generalInformationForm.controls['CONFIRM_ON'].disable();
      }

      else if (this.e_type_val != '03' && this.e_type_val != '01' && this.e_type_val != '02') {
        //var date=moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY");
        var date_dump = (this.generalInformationForm.controls['JOIN_DT'].value != "") ? moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY").add(0, 'days').format('DD/MM/YYYY') : "";
        this.generalInformationForm.controls['SAN_FROM'].setValue(date_dump);
        this.generalInformationForm.controls['SAN_FROM'].disable();

      }


      else if (this.e_type_val = '02') {
        console.log("PROBATION-02");
        var date_dump = (this.generalInformationForm.controls['JOIN_DT'].value != "") ? moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY").add(0, 'days').format('DD/MM/YYYY') : "";
        //var date=moment(this.generalInformationForm.controls['JOIN_DT'].value, "DD/MM/YYYY");
        //console.log(date);
        this.generalInformationForm.controls['PROB_FROM'].setValue(date_dump);
        this.generalInformationForm.controls['PROB_FROM'].disable();


        var date3 = (this.generalInformationForm.controls['PROB_TO'].value != "") ? moment(this.generalInformationForm.controls['PROB_TO'].value, "DD/MM/YYYY").add(1, 'days').format('DD/MM/YYYY') : "";

        this.generalInformationForm.controls['CONFIRM_ON'].setValue(date3);
        this.generalInformationForm.controls['CONFIRM_ON'].disable();

      }
    }


  }


  TRA_DAYS_F(input_val) {
    if (this.generalInformationForm.controls['TRA_FROM'].value != "" && input_val != "") {
      //console.log(this.generalInformationForm.controls['TRA_FROM'].value);
      var date = moment(this.generalInformationForm.controls['TRA_FROM'].value, "DD/MM/YYYY").add(input_val, 'days').format('DD/MM/YYYY');
      //console.log(date);
      this.generalInformationForm.controls['TRA_TO'].setValue(date);

    }

  }

  TRA_DAYS_F_B() {
    if (this.generalInformationForm.controls['TRA_FROM'].value != "" && this.generalInformationForm.controls['TRA_TO'].value != "") {

      var cal_days = moment.duration(moment(this.generalInformationForm.controls['TRA_TO'].value, "DD/MM/YYYY").diff(moment(this.generalInformationForm.controls['TRA_FROM'].value, "DD/MM/YYYY"))).asDays()
      this.generalInformationForm.controls['TRA_DAYS'].setValue(cal_days);
      this.E_TYPE_F();
    }

  }




  SAN_DAYS_F(input_val) {
    if (this.generalInformationForm.controls['SAN_FROM'].value != "" && input_val != "") {
      //console.log(this.generalInformationForm.controls['TRA_FROM'].value);
      var date = moment(this.generalInformationForm.controls['SAN_FROM'].value, "DD/MM/YYYY").add(input_val, 'days').format('DD/MM/YYYY');
      //console.log(date);
      this.generalInformationForm.controls['SAN_TO'].setValue(date);

    }

  }

  SAN_DAYS_F_B() {
    if (this.generalInformationForm.controls['SAN_FROM'].value != "" && this.generalInformationForm.controls['SAN_TO'].value != "") {

      var cal_days = moment.duration(moment(this.generalInformationForm.controls['SAN_TO'].value, "DD/MM/YYYY").diff(moment(this.generalInformationForm.controls['SAN_FROM'].value, "DD/MM/YYYY"))).asDays()
      this.generalInformationForm.controls['SAN_DAYS'].setValue(cal_days);
      this.E_TYPE_F();
    }

  }


  PROB_DAYS_F(input_val) {
    if (this.generalInformationForm.controls['PROB_FROM'].value != "" && input_val != "") {
      //console.log(this.generalInformationForm.controls['TRA_FROM'].value);
      var date = moment(this.generalInformationForm.controls['PROB_FROM'].value, "DD/MM/YYYY").add(input_val, 'days').format('DD/MM/YYYY');
      //console.log(date);
      this.generalInformationForm.controls['PROB_TO'].setValue(date);

    }

  }

  PROB_DAYS_F_B() {
    if (this.generalInformationForm.controls['PROB_FROM'].value != "" && this.generalInformationForm.controls['PROB_TO'].value != "") {

      var cal_days = moment.duration(moment(this.generalInformationForm.controls['PROB_TO'].value, "DD/MM/YYYY").diff(moment(this.generalInformationForm.controls['PROB_FROM'].value, "DD/MM/YYYY"))).asDays()
      this.generalInformationForm.controls['PROB_DAYS'].setValue(cal_days);
      this.E_TYPE_F();
    }

  }


  generateEmpCode(isChecked) {

    if (isChecked == true) {
      this.appDataService.getLatestECODEValue({ "txt_Branch": this.loggedUserObj.Branch }).subscribe(data => {
        this.generalInformationForm.patchValue({
          E_CODE: data[0].L_E_CODE
        });
        this.generalInformationForm.controls['E_CODE'].disable();
      });
    }
    else {
      this.generalInformationForm.patchValue({
        E_CODE: ''
      });
      this.generalInformationForm.controls['E_CODE'].enable();
    }


  }

  fieldRequiredControls() {

  }

  navigatePrevious() {

  }

  /*validateAllFormFields(){
    var fieldsControls = this.generalInformationForm.controls;
    for (let field in fieldsControls) {
      const control = this.generalInformationForm.get(field);
      control.markAsTouched({ onlySelf: true });
    }
  }*/

  validateAllFormControl(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormControl(control);
      }
    });
  }

  validateAllFormFields() {
    var fieldsControls = this.generalInformationForm.controls;
    for (let field in fieldsControls) {
      console.log(field);
      const control = this.generalInformationForm.get(field);
      control.markAsTouched({ onlySelf: true });
    }
  }

  validateAllFormFields_WRK() {
    var fieldsControls = this.generalInformationForm.controls;
    //console.log("req>>>"+this.requiredFields);     
    for (let field in this.requiredFields) {
      // console.log(this.requiredFields[field]);     
      const control = this.generalInformationForm.get(this.requiredFields[field]);
      if (control != null) {
        /*if( this.generalInformationForm.controls[this.requiredFields[field]].value=="")
        {
          console.log(this.requiredFields[field]);
          this.generalInformationForm.controls[this.requiredFields[field]].setValue("");
        }  */
        control.markAsTouched({ onlySelf: true });
      }
    }
  }


  isFieldValid(field: string) {
    //console.log("sfsfsfs>>"+this.generalInformationForm.enabled);
    return this.generalInformationForm.get(field).enabled && !this.generalInformationForm.get(field).valid && (this.generalInformationForm.get(field).dirty || this.generalInformationForm.get(field).touched);
  }

  saveGeneralInformation() {

    this.validateAllFormFields_WRK();

    // this.validateAllFormControl(this.generalInformationForm);
    this.spinner.show();

    var formData: any = this.generalInformationForm.getRawValue();

    if (this.generalInformationForm.status == "VALID") {
      this.requestPost.ECODE = this.eCodeVal;
      this.requestPost.CreateBy = this.loggedUserObj.E_CODE;
      this.requestPost.UpdateBy = this.loggedUserObj.E_CODE;
      // this.requestPost.Branch = this.generalInfoData.Branch.VAL;
      this.requestPost.Branch = this.loggedUserObj.Branch;
      this.requestPost.RecordType = (this.createNewFlag) ? "New" : "Update";
      console.log(this.createNewFlag);
      var genralFormData: any = Object.assign(formData, this.requestPost);
      this.appDataService.saveGeneralInformation(genralFormData).subscribe(response => {
        if (response.Code == "True") {
          //this.showSuccess(this.staticMsgs["success_messages"].sucess_general_info);
          this.showSuccess(response.MSG);
        }
        else {
          //this.showError(this.staticMsgs["error_messages"].error_general_info);
          this.showError(response.MSG);
        }
        this.spinner.hide();
      }, err => {
        //this.showError(this.staticMsgs["error_messages"].error_general_info);
        this.spinner.hide();
        this.showError("Web Service Error... 409");
      });
    }
    else {
      this.showError("Form Validation Failed....");
      this.spinner.hide();
    }
  }

  /** Toast Message Starts **/
  showSuccess(msg) {
    this.messageService.add({ severity: 'success', summary: 'Success Message', detail: msg });
  }
  showError(msg) {
    this.messageService.add({ severity: 'error', summary: 'Error Message', detail: msg });
  }
  /** Toast Message Ends **/

  onImageSelect(event) {
    let me = this;
    let file = event.target.files[0];
    let fileName = event.target.files[1];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event: any) => {
      this.imageSelectedString = event.target.result;
    }
    reader.onerror = function (error) {

    };
    this.imageSelectedName = (file) ? file.name : "";

    this.requestPost.ECODE = this.eCodeVal;
    this.requestPost.CreateBy = this.loggedUserObj.E_CODE;
    this.requestPost.UpdateBy = this.loggedUserObj.E_CODE;
    this.requestPost.Branch = this.loggedUserObj.Branch;
    this.requestPost.RecordType = "Update";
  }

  onImageSelectedSave() {
    this.spinner.show();
    var profileImgObj: any = Object.assign({ IMAGE: this.imageSelectedString, IMAGE_TYPE: this.imageSelectedName }, this.requestPost);
    this.appDataService.saveProfileImage(profileImgObj).subscribe(response => {
      if (response.Code == "True") {
        this.userBasicDetail(this.userObj);
        this.showSuccess(response.MSG);
        //this.showSuccess(this.staticMsgs["success_messages"].sucess_general_info);
      }
      else {
        this.showError(response.MSG);
        //this.showError(this.staticMsgs["error_messages"].error_general_info);
      }
      this.spinner.hide();
    }, err => {
      this.showError("Web Service Error... 409");
      //this.showError(this.staticMsgs["error_messages"].error_general_info);
      this.spinner.hide();
    });
  }

  /*
  ngOnInit() {
    this.visibleDataArr = this.filterFormData(this.dataArr);
    console.log("visibleDataArr Obj-->",this.visibleDataArr);
    this.prepareDynamicReactForm(this.visibleDataArr);
  }

  filterFormData(dataArr){
    var visibleFieldArr:any = [];
    dataArr.forEach(element => {
      if(element.UV == null){
        visibleFieldArr.push(element);
      }
    });

    return visibleFieldArr;
  }

  prepareDynamicReactForm(visibleDataArr){

    if(visibleDataArr.length > 0){
      var formControlObj:any = [];
      visibleDataArr.forEach(element => {
        let formCtlName:any = " "+element.TAG +" : ['']";
        formControlObj.push(formCtlName);
      });
    }

    formControlObj = JSON.stringify(formControlObj);
    formControlObj = formControlObj.replace(/["]+/g, '');       //replace all double quotes from string
    formControlObj = formControlObj.replace(/^\[(.+)\]$/,'$1'); //replace first and last square bracket from string
    var formControlObj1:object = eval('({' + formControlObj + '})');  //eval JS function to creating object

    this.generalInformationForm = this._fb.group(formControlObj1);

    console.log(this.generalInformationForm);
  } */

}
