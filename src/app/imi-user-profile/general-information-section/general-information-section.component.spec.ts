import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralInformationSectionComponent } from './general-information-section.component';

describe('GeneralInformationSectionComponent', () => {
  let component: GeneralInformationSectionComponent;
  let fixture: ComponentFixture<GeneralInformationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralInformationSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralInformationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
