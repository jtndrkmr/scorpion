import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchableListingComponent } from './searchable-listing.component';

describe('SearchableListingComponent', () => {
  let component: SearchableListingComponent;
  let fixture: ComponentFixture<SearchableListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchableListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchableListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
