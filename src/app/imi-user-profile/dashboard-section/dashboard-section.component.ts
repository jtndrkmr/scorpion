import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-section',
  templateUrl: './dashboard-section.component.html',
  styleUrls: ['./dashboard-section.component.css']
})
export class DashboardSectionComponent implements OnInit {

  router: string;
  editUserInfoData:any;

  constructor(
    public _router: Router
  ) {
    //this.router = _router.url;
  }

  ngOnInit() {
  }

  pushExistingUserEditInfo(data:any){
    this.editUserInfoData = data;
  }

}
