import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactInformationSectionComponent } from './contact-information-section.component';

describe('ContactInformationSectionComponent', () => {
  let component: ContactInformationSectionComponent;
  let fixture: ComponentFixture<ContactInformationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactInformationSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactInformationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
