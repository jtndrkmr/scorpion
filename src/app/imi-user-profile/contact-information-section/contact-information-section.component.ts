import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppDataService } from './../../_services/user-data-service';
import { DataListingService } from './../../_services/data-listing-service';
import { SharedService } from './../../_services/shared-service';
import { RequestPost } from './../../models/imi-request-post-model';
import {MessageService} from 'primeng/api';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'imi-contact-information-section',
  templateUrl: './contact-information-section.component.html',
  styleUrls: ['./contact-information-section.component.css']
})

export class ContactInformationSectionComponent implements OnInit {
  contactInfoForm:FormGroup;
  requestPost = new RequestPost();
  loggedUserObj:any;
  editableUserObj:any;
  role:any;
  userObj:any;
  staticMsgs:any;
  citylist:any= []; 
  statelist:any= []; ;

  constructor(
    private _fb : FormBuilder,
    private appDataService: AppDataService,
    private dataListingService: DataListingService,
    private sharedService: SharedService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.prepareForm();

    this.editableUserObj = this.sharedService.editableFlag.value;
    this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
    console.log(this.editableUserObj);
    if(this.editableUserObj != false && this.editableUserObj !=undefined){
      this.userObj = {"txt_Branch":this.editableUserObj.EU_BRANCH, "txt_UserID": this.editableUserObj.EU_CODE};
    }else{
      this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
    }
    this.StaticListRecord(this.loggedUserObj.Branch);
    this.role = this.loggedUserObj.ROLE;
    let menuFamily = {"txt_Menu_Type":"CNTNFO"};

    this.contactInformation(this.userObj, menuFamily);

    this.staticMsgs = this.sharedService.getStaticMessages();
  }

  prepareForm(){
    this.contactInfoForm = this._fb.group({
      ADD_1: [''],
      ADD_2: [''],
      ADD_3: [''],
      CITY_CD: [''],
      STATE_CD: [''],
      PIN: [''],
      PERADD_1: [''],
      PERADD_2: [''],
      PERADD_3: [''],
      PERCTY_CD: [''],
      PERST_CD: [''],
      PERPIN: [''],
      CNTCT_NO1: [''],
      CNTCT_NO2: [''],
      Ref_Name: [''],
      Ref_Contact_No: [''],
      Ref_Remarks: [''],
      EMERGENCY_CONTACT_NAME: [''],
      EMERGENCY_CONTACT_NO: [''],
      EMERGENCY_CONTACT_RELATION: ['']
    })
  }

  contactInformation(userObj, menuCnt){
    const contactInfoObj = Object.assign(userObj, menuCnt);
    this.appDataService.getContactInformationDetails(contactInfoObj).subscribe(response=>{
      //console.log("Tada contact -->",response);
      var contactObj = response;
      this.patchContactData(contactObj);
    });
  }

  patchContactData(contactObj){
    this.contactInfoForm.patchValue({
      ADD_1: contactObj.ADD_1.VAL,
      ADD_2: contactObj.ADD_2.VAL,
      ADD_3: contactObj.ADD_3.VAL,
      CITY_CD: contactObj.CITY_CD.VAL,
      STATE_CD: contactObj.STATE_CD.VAL,
      PIN: contactObj.PIN.VAL,
      PERADD_1: contactObj.PERADD_1.VAL,
      PERADD_2: contactObj.PERADD_2.VAL,
      PERADD_3: contactObj.PERADD_3.VAL,
      PERCTY_CD: contactObj.PERST_CD.VAL,
      PERST_CD: contactObj.PERST_CD.VAL,
      PERPIN: contactObj.PERPIN.VAL,
      CNTCT_NO1: contactObj.CNTCT_NO1.VAL,
      CNTCT_NO2: contactObj.CNTCT_NO2.VAL,
      Ref_Name: contactObj.Ref_Name.VAL,
      Ref_Contact_No: contactObj.Ref_Contact_No.VAL,
      Ref_Remarks: contactObj.Ref_Remarks.VAL,
      EMERGENCY_CONTACT_NAME: contactObj.EMERGENCY_CONTACT_NAME.VAL,
      EMERGENCY_CONTACT_NO: contactObj.EMERGENCY_CONTACT_NO.VAL,
      EMERGENCY_CONTACT_RELATION: contactObj.EMERGENCY_CONTACT_RELATION.VAL
    });
  }

  saveContactInformation(){
    this.spinner.show();
    var formData:any = this.contactInfoForm.getRawValue();
    this.requestPost.ECODE = this.userObj.txt_UserID;
    this.requestPost.CreateBy = this.loggedUserObj.E_CODE;
    this.requestPost.UpdateBy = this.loggedUserObj.E_CODE;
    this.requestPost.Branch = this.userObj.txt_Branch;
    this.requestPost.RecordType = 'U';

    var contactFormDataObj:any = Object.assign(formData, this.requestPost);

    this.appDataService.saveContactInformation(contactFormDataObj).subscribe(response=>{
      if(response.Code=="True"){
        this.showSuccess(response.MSG);
      }else{
        this.showError(response.MSG);
      }
      this.spinner.hide();
    },err=>{
      this.spinner.hide();
      this.showError("Web Service Error... 409");
    });
  }


  StaticListRecord(branch){
    var reqObj:any = {"txt_Branch":branch, "txt_Type":"GI_ALL"};
    this.dataListingService.getStaticValuesList(reqObj).subscribe(response=>{
      this.citylist = response.GI_JU; 
      this.statelist = response.GI_STATE; 
    });
    
  }

  /** Toast Message Starts **/
  showSuccess(msg) {
    this.messageService.add({severity:'success', summary: 'Success Message', detail:msg});
  }
  showError(msg) {
      this.messageService.add({severity:'error', summary: 'Error Message', detail:msg});
  }
  /** Toast Message Ends **/

}
