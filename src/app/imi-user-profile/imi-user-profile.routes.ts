import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { AuthGuard } from './../_guards/auth.guard';

import { DashboardSectionComponent } from './dashboard-section/dashboard-section.component';
import { UserProfileSectionComponent } from './user-profile-section/user-profile-section.component';
import { GeneralInformationSectionComponent } from './general-information-section/general-information-section.component';
import { UsersListingSectionComponent } from './users-listing-section/users-listing-section.component';
import { SearchableListingComponent } from './searchable-listing/searchable-listing.component';
import { SalaryListingSectionComponent } from './salary-listing-section/salary-listing-section.component';

const routes: Routes = [
    {
        path: '', 
        component:DashboardSectionComponent,
        children:[
            {path: 'user-profile', component:DashboardSectionComponent},
            {path: 'general-info', component:GeneralInformationSectionComponent},
            {path: 'users-listing', component:UsersListingSectionComponent},
            {path: 'salary-listing', component:SalaryListingSectionComponent},
            {path: 'salary', component:SearchableListingComponent}
        ]
    }
    
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ImiUserProfileRoute{
}
