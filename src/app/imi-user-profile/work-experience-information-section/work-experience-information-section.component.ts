import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppDataService } from './../../_services/user-data-service';
import { DataListingService } from './../../_services/data-listing-service';
import { SharedService } from './../../_services/shared-service';
import { RequestPost } from './../../models/imi-request-post-model';
import * as moment from 'moment';
import { NgxSpinnerService } from "ngx-spinner";
import { MessageService } from 'primeng/api';

@Component({
  selector: 'imi-work-experience-information-section',
  templateUrl: './work-experience-information-section.component.html',
  styleUrls: ['./work-experience-information-section.component.css']
})
export class WorkExperienceInformationSectionComponent implements OnInit {

  workExperInfoForm:FormGroup;
  workExpInfoList:any;
  requestPost = new RequestPost();
  loggedUserObj:any;
  editableUserObj:any;
  role:any;
  userObj:any;
  staticMsgs:any;
  update_NWE_Flag:boolean = false;
  designationList:any = []; 
	
  constructor(
    private _fb : FormBuilder,
    private appDataService: AppDataService,
    private dataListingService: DataListingService,
    private sharedService: SharedService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private renderer2 : Renderer2,
  ) { }

  ngOnInit() {
  this.prepareForm();
  this.editableUserObj = this.sharedService.editableFlag.value;
  this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
  if(this.editableUserObj != false && this.editableUserObj !=undefined){
    this.userObj = {"txt_Branch":this.editableUserObj.EU_BRANCH, "txt_UserID": this.editableUserObj.EU_CODE};
  }else{
    this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
  }
  this.role = this.loggedUserObj.ROLE;
  let menuQualification = {"txt_Menu_Type":"WORKEXP"};

  this.workExperienceInformationData(this.userObj, menuQualification);
  this.staticMsgs = this.sharedService.getStaticMessages();
  this.StaticListRecord(this.loggedUserObj.Branch);
  }
  
  prepareForm(){
	this.workExperInfoForm = this._fb.group({
		EXP_YR_FR: [''],	
    EXP_YR_TO: [''],	
    EMPLOYER: [''],	
    EMP_ADD1: [''],	
    Joining_Designation_Code: [''],	
    Leaving_Designation_Code: [''],	
    Leaving_Salary: ['']
	});
  }

  patchDataWEForm(qualifiData, updateFlag){
    console.log("qualifiData tada-->",qualifiData.EXP_YR_FR);
    this.workExperInfoForm.patchValue({
      EXP_YR_FR: (qualifiData.EXP_YR_FR) ? moment(qualifiData.EXP_YR_FR, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      EXP_YR_TO: (qualifiData.EXP_YR_TO) ? moment(qualifiData.EXP_YR_TO, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      EMPLOYER: qualifiData.EMPLOYER,
      EMP_ADD1: qualifiData.EMP_ADD1,
      Joining_Designation_Code: qualifiData.Joining_Designation,
      Leaving_Designation_Code: qualifiData.Leaving_Designation,
      Leaving_Salary: qualifiData.Leaving_Salary
    });
    console.log("qualifiData tada-->",this.workExperInfoForm.getRawValue());
    this.update_NWE_Flag = updateFlag;
  }
  
  workExperienceInformationData(userObj, menuQuali){
    const workExpInfoObj = Object.assign(userObj, menuQuali);
    this.appDataService.getWorkExperienceListDetails(workExpInfoObj).subscribe(response=>{
      this.workExpInfoList = response;
    });

    this.requestPost.ECODE = this.userObj.txt_UserID;
    this.requestPost.CreateBy = this.loggedUserObj.E_CODE;
    this.requestPost.UpdateBy = this.loggedUserObj.E_CODE;
    this.requestPost.Branch = this.userObj.txt_Branch;
    this.requestPost.RecordType = (this.update_NWE_Flag)?'U':'I';
  }

  addnewWorkExpDetails(){
    this.patchDataWEForm('', false);
  }

  eidtWorkExperience(SRL){
    this.renderer2.addClass(document.querySelector('#WE_collapse'), "show");
    if(this.workExpInfoList.length>0){
      this.workExpInfoList.forEach(element => {
        if(element.SRL === SRL){
          this.patchDataWEForm(element, true);
        }
      });
    }
  }
  
  saveWorkExperienceInformation(){
	this.spinner.show();
    var workExpFormData:any = this.workExperInfoForm.getRawValue();
    var idDetailDataObj:any = Object.assign(workExpFormData, this.requestPost);
    this.appDataService.saveWorkExperienceInformation(idDetailDataObj).subscribe(response=>{
      if(response.Code=="True"){
        this.patchDataWEForm('', false);
        this.workExperienceInformationData(this.userObj, {"txt_Menu_Type":"TRING"});
        //this.showSuccess(this.staticMsgs["success_messages"].sucess_work_exp_info);
        this.showSuccess(response.MSG);
      }else{
        //this.showError(this.staticMsgs["error_messages"].error_work_exp_info);
        this.showError(response.MSG);
      }
      this.spinner.hide();
    },err=>{
      this.spinner.hide();
      this.showError("Web Service Error... 409");
      //this.showError(this.staticMsgs["error_messages"].error_work_exp_info);
    });
  }

  StaticListRecord(branch){
    var reqObj:any = {"txt_Branch":branch, "txt_Type":"GI_ALL"};
    this.dataListingService.getStaticValuesList(reqObj).subscribe(response=>{
      this.designationList = response.GI_DESIG; 
    });
  }
  
  /** Toast Message Starts **/
  showSuccess(msg) {
    this.messageService.add({severity:'success', summary: 'Success Message', detail:msg});
  }
  showError(msg) {
      this.messageService.add({severity:'error', summary: 'Error Message', detail:msg});
  }
  /** Toast Message Ends **/

}