import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkExperienceInformationSectionComponent } from './work-experience-information-section.component';

describe('WorkExperienceInformationSectionComponent', () => {
  let component: WorkExperienceInformationSectionComponent;
  let fixture: ComponentFixture<WorkExperienceInformationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkExperienceInformationSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkExperienceInformationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
