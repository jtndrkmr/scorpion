import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingInformationSectionComponent } from './training-information-section.component';

describe('TrainingInformationSectionComponent', () => {
  let component: TrainingInformationSectionComponent;
  let fixture: ComponentFixture<TrainingInformationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingInformationSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingInformationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
