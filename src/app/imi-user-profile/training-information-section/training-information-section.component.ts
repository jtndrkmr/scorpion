import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppDataService } from './../../_services/user-data-service';
import { DataListingService } from './../../_services/data-listing-service';
import { SharedService } from './../../_services/shared-service';
import { RequestPost } from './../../models/imi-request-post-model';
import * as moment from 'moment';
import { NgxSpinnerService } from "ngx-spinner";
import { MessageService } from 'primeng/api';

@Component({
  selector: 'imi-training-information-section',
  templateUrl: './training-information-section.component.html',
  styleUrls: ['./training-information-section.component.css']
})
export class TrainingInformationSectionComponent implements OnInit {
  
  trainingInformationForm:FormGroup;
  trainingInfoList:any;
  requestPost = new RequestPost();
  loggedUserObj:any;
  editableUserObj:any;
  role:any;
  userObj:any;
  staticMsgs:any;
  updateFlag:boolean = false;
	
  constructor(
    private _fb : FormBuilder,
    private appDataService: AppDataService,
    private dataListingService: DataListingService,
    private sharedService: SharedService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private renderer2 : Renderer2
  ) { }

  ngOnInit() {
  this.prepareForm();
  this.editableUserObj = this.sharedService.editableFlag.value;
  this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
  if(this.editableUserObj != false && this.editableUserObj !=undefined){
    this.userObj = {"txt_Branch":this.editableUserObj.EU_BRANCH, "txt_UserID": this.editableUserObj.EU_CODE};
  }else{
    this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
  }
  this.role = this.loggedUserObj.ROLE;
  let menuQualification = {"txt_Menu_Type":"TRING"};

  this.idDetailsInformationData(this.userObj, menuQualification);
  this.staticMsgs = this.sharedService.getStaticMessages();
  }
  
  prepareForm(){
    this.trainingInformationForm = this._fb.group({
      DATE_FROM: [''],
      DATE_TO: [''],
      INSTITUTE: [''],
      TRAINING_NAME: [''],
      TRAINING_DETAILS: ['']
    });
  }

  patchDataForm(qualifiData, updateFlag){
    this.trainingInformationForm.patchValue({
      DATE_FROM: (qualifiData.DATE_FROM) ? moment(qualifiData.DATE_FROM, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      DATE_TO: (qualifiData.DATE_TO) ? moment(qualifiData.DATE_TO, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      INSTITUTE: qualifiData.INSTITUTE,
      TRAINING_NAME: qualifiData.TRAINING_NAME,
      TRAINING_DETAILS: qualifiData.TRAINING_DETAILS
    });
    this.updateFlag = updateFlag;
  }
  
  idDetailsInformationData(userObj, menuQuali){
    
    const idDetailInfoObj = Object.assign(userObj, menuQuali);
    this.appDataService.geTrainingListDetails(idDetailInfoObj).subscribe(response=>{
      this.trainingInfoList = response;
    });

    this.requestPost.ECODE = this.userObj.txt_UserID;
    this.requestPost.CreateBy = this.loggedUserObj.E_CODE;
    this.requestPost.UpdateBy = this.loggedUserObj.E_CODE;
    this.requestPost.Branch = this.userObj.txt_Branch;
    this.requestPost.RecordType = (this.updateFlag)?'U':'I';
  }

  addnewTrainingDetail(){
    this.patchDataForm('', false);
  }

  eidtTraining(SRL){
    this.renderer2.addClass(document.querySelector('#TI_collapse'), "show");
    if(this.trainingInfoList.length>0){
      this.trainingInfoList.forEach(element => {
        if(element.SRL === SRL){
          this.patchDataForm(element, true);
        }
      });
    }
  }
  
  saveTrainingInformation(){
	this.spinner.show();
    var formData:any = this.trainingInformationForm.getRawValue();
    var triningDataObj:any = Object.assign(formData, this.requestPost);
    this.appDataService.saveTrainingInformation(triningDataObj).subscribe(response=>{
      if(response.Code=="True"){
        this.patchDataForm('', false);
        this.idDetailsInformationData(this.userObj, {"txt_Menu_Type":"TRING"});
        this.showSuccess(response.MSG);
        //this.showSuccess(this.staticMsgs["success_messages"].sucess_training_info);
      }else{
        this.showError(response.MSG);
        //this.showError(this.staticMsgs["error_messages"].error_training_info);
      }
      this.spinner.hide();
    },err=>{
      this.spinner.hide();
      this.showError("Web Service Error... 409");
      //this.showError(this.staticMsgs["error_messages"].error_training_info);
    });
  }
  
  /** Toast Message Starts **/
  showSuccess(msg) {
    this.messageService.add({severity:'success', summary: 'Success Message', detail:msg});
  }
  showError(msg) {
      this.messageService.add({severity:'error', summary: 'Error Message', detail:msg});
  }
  /** Toast Message Ends **/

}
