import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppDataService } from './../../_services/user-data-service';
import { DataListingService } from './../../_services/data-listing-service';
import { SharedService } from './../../_services/shared-service';
import { RequestPost } from './../../models/imi-request-post-model';
import * as moment from 'moment';
import {MessageService} from 'primeng/api';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'imi-family-information-section',
  templateUrl: './family-information-section.component.html',
  styleUrls: ['./family-information-section.component.css'],
  providers: [MessageService]
})
export class FamilyInformationSectionComponent implements OnInit {

  FamilyInformationForm:FormGroup;
  childInformationForm:FormGroup;
  requestPost = new RequestPost();
  loggedUserObj:any;
  editableUserObj:any;
  role:any;
  userObj:any;
  menuFamily:any;
  familyInfoData:any;
  parentsDetails:any;
  childrenDetails:any;
  staticMsgs:any;
  requiredFields:any []=["FA_NAME"];
  requiredFields_Child:any []=["CH_NAME","CH_SEX","D_O_B"];


  constructor(
    private _fb : FormBuilder,
    private appDataService: AppDataService,
    private dataListingService: DataListingService,
    private sharedService: SharedService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.prepareReactForm();
    this.editableUserObj = this.sharedService.editableFlag.value;
    this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
    console.log(this.editableUserObj);
    if(this.editableUserObj != false && this.editableUserObj !=undefined){
      this.userObj = {"txt_Branch":this.editableUserObj.EU_BRANCH, "txt_UserID": this.editableUserObj.EU_CODE};
    }else{
      this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
    }

    this.role = this.loggedUserObj.ROLE;
    this.menuFamily = {"txt_Menu_Type":"FINFO"};

    this.familyInformation(this.userObj, this.menuFamily);

    this.staticMsgs = this.sharedService.getStaticMessages();
     this.set_form_validation();
     this.set_Child_form_validation();
    
  }

  

  prepareReactForm(){
    this.FamilyInformationForm = this._fb.group({
      FA_NAME: [''],
      F_OC_CD: [''],
      FA_DOB: [''],
      MO_NAME: [''],
      MO_OC_CD: [''],
      MO_DOB: [''],
      SPOUSE: [''],
      SP_OC_CD: [''],
      SP_DOB: [''],
      SP_DOM: ['']
    });
    this.childInformationForm = this._fb.group({
      CH_NAME: [''],
      CH_SEX: [''],
      D_O_B: ['']
    });
  }

  familyInformation(userObj, menuFamily){
    const familInfoObj = Object.assign(userObj, menuFamily);
    this.appDataService.getFamilyInformationDetails(familInfoObj).subscribe(response=>{
      this.familyInfoData = response;
      this.parentsDetails = this.familyInfoData.FMS;
      this.childrenDetails = this.familyInfoData.Children_Details;

      this.requestPost.ECODE = this.userObj.txt_UserID;
      this.requestPost.CreateBy = this.loggedUserObj.E_CODE;
      this.requestPost.UpdateBy = this.loggedUserObj.E_CODE;
      this.requestPost.Branch = this.userObj.txt_Branch;
      this.requestPost.RecordType = (this.parentsDetails != [])?'U':'I';
      if(this.parentsDetails != []){
        this.parentsDetails = this.parentsDetails[0];
        this.patchParentsDetails(this.parentsDetails);
      }

    });
  }

  patchParentsDetails(familyData){
    this.FamilyInformationForm.patchValue({
      FA_NAME: familyData.FA_NAME,
      F_OC_CD: familyData.F_OC_CD,
      FA_DOB:  (familyData.FA_DOB) ? moment(familyData.FA_DOB, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      MO_NAME: familyData.MO_NAME,
      MO_OC_CD: familyData.MO_OC_CD,
      MO_DOB:  (familyData.MO_DOB) ? moment(familyData.MO_DOB, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      SPOUSE: familyData.SPOUSE,
      SP_OC_CD: familyData.SP_OC_CD,
      SP_DOB:  (familyData.SP_DOB) ? moment(familyData.SP_DOB, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      SP_DOM:  (familyData.SP_DOM) ? moment(familyData.SP_DOM, 'YYYY-MM-DD').format('DD/MM/YYYY') : ''
    });
  }

  isFieldValid(field: string,form) {
    return this.FamilyInformationForm.get(field).enabled && !this.FamilyInformationForm.get(field).valid && this.FamilyInformationForm.get(field).touched;
  }
  isFieldValid_Child(field: string,form) {
    return this.childInformationForm.get(field).enabled && !this.childInformationForm.get(field).valid && this.childInformationForm.get(field).touched;
  }

  set_form_validation()
  {
    for (let field in this.requiredFields) {
      this.FamilyInformationForm.controls[this.requiredFields[field]].setValidators([Validators.required]);
    }
  }

  set_Child_form_validation()
  {
    for (let field in this.requiredFields_Child) {
      this.childInformationForm.controls[this.requiredFields_Child[field]].setValidators([Validators.required]);
    }
  }


  validateAllFormChildFields(){   
    for (let field in this.requiredFields_Child) {
     // this.FamilyInformationForm.controls[this.requiredFields[field]].setValidators([Validators.required]);
      const control = this.childInformationForm.get(this.requiredFields_Child[field]);
      if(control!=null)
      {            
     control.markAsTouched({ onlySelf: true });
      }     
    }
  }

  validateAllFormFamilyFields(){
   
    for (let field in this.requiredFields) {
     // this.FamilyInformationForm.controls[this.requiredFields[field]].setValidators([Validators.required]);
      const control = this.FamilyInformationForm.get(this.requiredFields[field]);
      if(control!=null)
      {            
     control.markAsTouched({ onlySelf: true });
      }     
    }
  }

  saveFamilyParentsInformation(){
    this.spinner.show();
    this.validateAllFormFamilyFields()
    var formData:any = this.FamilyInformationForm.getRawValue();
    if(this.FamilyInformationForm.status == "VALID"){

    var familyFormDataObj:any = Object.assign(formData, this.requestPost);

    this.appDataService.saveFamilyParentsInformation(familyFormDataObj).subscribe(response=>{
      if(response.Code=="True"){            
        this.familyInformation(this.userObj, this.menuFamily); 
        this.showSuccess(response.MSG);      
        //this.showError(this.staticMsgs["success_messages"].sucess_family_info);
      }else{
        this.showError(response.MSG);
        //this.showError(this.staticMsgs["error_messages"].error_family_info);
      }
      this.spinner.hide();
    },err=>{
      this.spinner.hide();
      this.showError("Web Service Error... 409");      
      //this.showError(this.error_messagess);
      //this.showError(this.staticMsgs["error_messages"].error_family_info);
    });
  }
  else
  {
    this.spinner.hide();
    this.showError("Form Validation Failed....");
  }
  }

  saveFamilyChildInformation(){
    this.spinner.show();
    this.validateAllFormChildFields();    
    if(this.childInformationForm.status == "VALID"){
    var formData:any = this.childInformationForm.getRawValue();
    var childFormDataObj:any = Object.assign(formData, this.requestPost);
    this.appDataService.saveFamilyChildrenInformation(childFormDataObj).subscribe(response=>{
      if(response.Code=="True"){ 
        this.childInformationForm.controls['CH_NAME'].setValue('');
        this.childInformationForm.controls['CH_SEX'].setValue('');
        this.childInformationForm.controls['D_O_B'].setValue('');   
        this.familyInformation(this.userObj, this.menuFamily);
        //this.showSuccess(this.staticMsgs["success_messages"].sucess_child_info);
        this.showSuccess(response.MSG);    
      }else{
        //this.showError(this.staticMsgs["error_messages"].error_child_info);
        this.showError(response.MSG);
      }
      this.spinner.hide();
    },err=>{
      this.spinner.hide();
      this.showError("Web Service Error... 409");      
      //this.showError(this.staticMsgs["error_messages"].error_child_info);
    });
  }
  else
  {
    this.spinner.hide();
    this.showError("Form Validation Failed....");
  }

  }

  delChildren(CH_ID){  

    var childFormDataObj:any = Object.assign({'CH_ID':CH_ID}, this.requestPost);
    this.appDataService.delFamilyChildrenInformation(childFormDataObj).subscribe(response=>{
      if(response.Code=="True"){
        this.familyInformation(this.userObj, this.menuFamily);
        this.showSuccess(response.MSG);   
      }else{
        this.showError(response.MSG);
      }
      this.spinner.hide();
    },err=>{
      this.spinner.hide();
      this.showError("Web Service Error... 409");  
    });

  }

  /** Toast Message Starts **/
  showSuccess(msg) {
    this.messageService.add({severity:'success', summary: 'Success Message', detail:msg});
  }
  showError(msg) {
      this.messageService.add({severity:'error', summary: 'Error Message', detail:msg});
  }
  /** Toast Message Ends **/
}
