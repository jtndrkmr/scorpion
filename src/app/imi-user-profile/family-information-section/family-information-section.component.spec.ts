import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyInformationSectionComponent } from './family-information-section.component';

describe('FamilyInformationSectionComponent', () => {
  let component: FamilyInformationSectionComponent;
  let fixture: ComponentFixture<FamilyInformationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyInformationSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyInformationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
