import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersListingSectionComponent } from './users-listing-section.component';

describe('UsersListingSectionComponent', () => {
  let component: UsersListingSectionComponent;
  let fixture: ComponentFixture<UsersListingSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersListingSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersListingSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
