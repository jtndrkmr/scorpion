import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdDetailsInformationSectionComponent } from './id-details-information-section.component';

describe('IdDetailsInformationSectionComponent', () => {
  let component: IdDetailsInformationSectionComponent;
  let fixture: ComponentFixture<IdDetailsInformationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdDetailsInformationSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdDetailsInformationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
