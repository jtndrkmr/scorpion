import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppDataService } from './../../_services/user-data-service';
import { DataListingService } from './../../_services/data-listing-service';
import { SharedService } from './../../_services/shared-service';
import { RequestPost } from './../../models/imi-request-post-model';
import * as moment from 'moment';
import { NgxSpinnerService } from "ngx-spinner";
import { MessageService } from 'primeng/api';
import { PATTERN_VALIDATOR } from '@angular/forms/src/directives/validators';

@Component({
  selector: 'imi-id-details-information-section',
  templateUrl: './id-details-information-section.component.html',
  styleUrls: ['./id-details-information-section.component.css']
})
export class IdDetailsInformationSectionComponent implements OnInit {

  idDetailsInformationForm:FormGroup;
  idDetailsInfoList:any;
  requestPost = new RequestPost();
  loggedUserObj:any;
  editableUserObj:any;
  role:any;
  userObj:any;
  staticMsgs:any;
  updateFlag:boolean = false;
  idLists:any = []; 
  selectedDocumentString:any;
  requiredFields:any []=["ID_Type","ID_Number"];
	ID_Number_error:any;
  constructor(
    private _fb : FormBuilder,
    private renderer2 : Renderer2,
    private sanitizer: DomSanitizer,
    private appDataService: AppDataService,
    private dataListingService: DataListingService,
    private sharedService: SharedService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
  this.prepareForm();
  this.editableUserObj = this.sharedService.editableFlag.value;
  this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
  if(this.editableUserObj != false && this.editableUserObj !=undefined){
    this.userObj = {"txt_Branch":this.editableUserObj.EU_BRANCH, "txt_UserID": this.editableUserObj.EU_CODE};
  }else{
    this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
  }
  this.role = this.loggedUserObj.ROLE;
  let menuQualification = {"txt_Menu_Type":"IDD"};
  this.idDetailsInformationData(this.userObj, menuQualification);

  this.staticMsgs = this.sharedService.getStaticMessages();
  this.StaticListRecord(this.loggedUserObj.Branch);
  this.set_form_validation();
  }

  StaticListRecord(branch){
    var reqObj:any = {"txt_Branch":branch, "txt_Type":"GI_ALL"};
    this.dataListingService.getStaticValuesList(reqObj).subscribe(response=>{
      this.idLists = response.GI_ID; 
    });
  }
  
  prepareForm(){
	this.idDetailsInformationForm = this._fb.group({
		ID_Type: [''],
		ID_Number: [''],
		Date_Issued: [''],
		Issuing_Authority: [''],
		Place_Issued: [''],
		Country_Issued: [''],
    Valid_Upto: [''],
    DOC: ['']
	});
  }

  onFileChanges(event) {
    let me = this;
    let file = event.target.files[0];
    let fileName = event.target.files[1];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event:any) => {
      this.selectedDocumentString = event.target.result;
      this.idDetailsInformationForm.patchValue({
        DOC: this.selectedDocumentString
      });
    }
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }

  patchDataForm(qualifiData, updateFlag){
    this.idDetailsInformationForm.patchValue({
      ID_Type: qualifiData.ID_Type,
      ID_Number: qualifiData.ID_Number,
      Date_Issued: (qualifiData.Date_Issued) ? moment(qualifiData.Date_Issued, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      Issuing_Authority: qualifiData.Issuing_Authority,
      Place_Issued: qualifiData.Place_Issued,
      Country_Issued: qualifiData.Country_Issued,
      Valid_Upto: (qualifiData.Valid_Upto) ? moment(qualifiData.Valid_Upto, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      DOC: (qualifiData.DOC) ? qualifiData.COURSE : ''
    });
    this.updateFlag = updateFlag;
  }
  
  idDetailsInformationData(userObj, menuQuali){
    const idDetailInfoObj = Object.assign(userObj, menuQuali);
    this.appDataService.geIdDetailsListDetails(idDetailInfoObj).subscribe(response=>{
      this.idDetailsInfoList = response;
    });
    this.requestPost.ECODE = this.userObj.txt_UserID;
    this.requestPost.CreateBy = this.loggedUserObj.E_CODE;
    this.requestPost.UpdateBy = this.loggedUserObj.E_CODE;
    this.requestPost.Branch = this.userObj.txt_Branch;
    this.requestPost.RecordType = (this.updateFlag)?'U':'I';
  }

  addnewIdDetails(){
    this.patchDataForm('', false);
  }

  eidtQualification(SRL){
    this.renderer2.addClass(document.querySelector('#ID_collapse'), "show");
    if(this.idDetailsInfoList.length>0){
      this.idDetailsInfoList.forEach(element => {
        if(element.RecID === SRL){
          this.patchDataForm(element, true);
        }
      });
    }
  }

  ID_CHANGE()
  {
  var ITYPEVAL=this.idDetailsInformationForm.controls['ID_Type'].value;    
 // console.log(ITYPEVAL);
    if(ITYPEVAL=="01")
    {  
      this.idDetailsInformationForm.controls['ID_Number'].setValidators(null);    
      this.idDetailsInformationForm.controls['ID_Number'].setValidators([Validators.required,Validators.pattern("^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$")]);   
     
    }
    else if(ITYPEVAL=="06"){
      this.idDetailsInformationForm.controls['ID_Number'].setValidators(null);
      this.idDetailsInformationForm.controls['ID_Number'].setValidators([Validators.required,Validators.pattern("[0-9]{12}")]); 
      
    }
   else{     
      this.idDetailsInformationForm.controls['ID_Number'].setValidators([Validators.required]);  
    }
    this.idDetailsInformationForm.controls['ID_Number'].setValue("");

  }

  
  isFieldValid(field: string,form) {    
    return this.idDetailsInformationForm.get(field).invalid && (this.idDetailsInformationForm.get(field).dirty || this.idDetailsInformationForm.get(field).touched) ;
  }

  /* // Orignal 
  isFieldValid(field: string,form) {
    return !this.idDetailsInformationForm.get(field).valid && this.idDetailsInformationForm.get(field).touched ;
  }
 isFieldValid_PAN(field: string,form) {
    var regex = new RegExp("^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$");
    var chek=regex.test(this.idDetailsInformationForm.get(field).value); 
    console.log(!this.idDetailsInformationForm.get(field).valid && this.idDetailsInformationForm.get(field).touched && chek==false);
    return !this.idDetailsInformationForm.get(field).valid && this.idDetailsInformationForm.get(field).touched && chek==false;
  }*/

  set_form_validation()
  {
    for (let field in this.requiredFields) {
      this.idDetailsInformationForm.controls[this.requiredFields[field]].setValidators([Validators.required]);
    }
  }

  validateAllFormIDFields(){
   
    for (let field in this.requiredFields) {
      const control = this.idDetailsInformationForm.get(this.requiredFields[field]);
      if(control!=null)
      {            
     control.markAsTouched({ onlySelf: true });
      }     
    }
  }
  
  saveIDdetailsInformation(){
  this.spinner.show();
  this.validateAllFormIDFields();

  if(this.idDetailsInformationForm.status == "VALID"){
    var formData:any = this.idDetailsInformationForm.getRawValue();
    var idDetailDataObj:any = Object.assign(formData, this.requestPost);
    this.appDataService.saveIdDetailInformation(idDetailDataObj).subscribe(response=>{
      if(response.Code=="True"){
        this.patchDataForm('', false);
        this.idDetailsInformationData(this.userObj, {"txt_Menu_Type":"IDD"});
        this.showSuccess(response.MSG);
        //this.showSuccess(this.staticMsgs["success_messages"].sucess_id_detail_info);
      }else{
        this.showError(response.MSG);
        //this.showError(this.staticMsgs["error_messages"].error_id_detail_info);
      }
      this.spinner.hide();
    },err=>{
      this.spinner.hide();
      this.showError("Web Service Error... 409");
      //this.showError(this.staticMsgs["error_messages"].error_id_detail_info);
    });
  }
  else
  {
    this.spinner.hide();
    this.showError("Form Validation Failed....");
  }
  }
  
  /** Toast Message Starts **/
  showSuccess(msg) {
    this.messageService.add({severity:'success', summary: 'Success Message', detail:msg});
  }
  showError(msg) {
      this.messageService.add({severity:'error', summary: 'Error Message', detail:msg});
  }
  /** Toast Message Ends **/

}