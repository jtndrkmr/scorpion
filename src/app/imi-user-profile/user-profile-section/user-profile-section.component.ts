import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppDataService } from './../../_services/user-data-service';
import { SharedService } from './../../_services/shared-service';

@Component({
  selector: 'imi-user-profile-section',
  templateUrl: './user-profile-section.component.html',
  styleUrls: ['./user-profile-section.component.css']
})
export class UserProfileSectionComponent implements OnInit {

  options = {
    responsive: true,
    maintainAspectRatio: true
  };
  data:any;
  demo_avator:any;

  public userObj:any;
  loggedUserObj:any;
  public personalInfoData:any;
  public profileimg_val:any;
  public personalAttendance:any;
  public birthdaysInfoData:any = [];

  constructor(
    private router: Router,
    private appDataService: AppDataService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
    this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
    //let menuGen = {"txt_Request_Type":"PR"};
    this.personalInformation(this.userObj);
   // this.demo_avator=this.
  }

  personalInformation(userObj){
    //const genInfoObj = Object.assign(userObj, menuGen);
    this.appDataService.getPersonalInformationDetails(userObj).subscribe(response=>{
        this.personalInfoData = response[0].Personal_Details[0];
        this.chartData(response[0].Leave_Attendance);
        this.personalAttendance = response[0].Attendance_Details;
        this.birthdaysInfoData = response[0].Birthday_Details;
        //console.log("personalInfoData tada->",this.birthdaysInfoData);
        this.convertingImage(this.loggedUserObj.User_Profile_Image, this.loggedUserObj.IMAGE_TYPE);
    });
  }

  convertingImage(blobImg,IMAGE_TYPE){
    let objectURL = 'data:image/'+IMAGE_TYPE+';base64,'+blobImg;
    this.profileimg_val = objectURL;
  }

  chartData(leaveAttendance){
    this.data = {
      labels: [leaveAttendance[0].Label, leaveAttendance[1].Label, leaveAttendance[2].Label, leaveAttendance[3].Label, leaveAttendance[4].Label],
      datasets: [
          {
              data: [leaveAttendance[0].Value, leaveAttendance[1].Value, leaveAttendance[2].Value, leaveAttendance[3].Value, leaveAttendance[4].Value],
              backgroundColor: [
                "#AED6F1",
                "#7DCEA0",
                "#F4D03F",
                "#EC7063",
                "#6C3483"
              ],
              hoverBackgroundColor: [
                "#AED6F1",
                "#7DCEA0",
                "#F4D03F",
                "#EC7063",
                "#6C3483"
              ]
          }]    
      };
  }

}
