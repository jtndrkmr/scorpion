import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QualificationInformationSectionComponent } from './qualification-information-section.component';

describe('QualificationInformationSectionComponent', () => {
  let component: QualificationInformationSectionComponent;
  let fixture: ComponentFixture<QualificationInformationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QualificationInformationSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QualificationInformationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
