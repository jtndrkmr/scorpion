import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppDataService } from './../../_services/user-data-service';
import { DataListingService } from './../../_services/data-listing-service';
import { SharedService } from './../../_services/shared-service';
import { RequestPost } from './../../models/imi-request-post-model';
import * as moment from 'moment';
import { NgxSpinnerService } from "ngx-spinner";
import { MessageService } from 'primeng/api';

@Component({
  selector: 'imi-qualification-information-section',
  templateUrl: './qualification-information-section.component.html',
  styleUrls: ['./qualification-information-section.component.css'],
  providers: [MessageService]
})
export class QualificationInformationSectionComponent implements OnInit {

	qualificationInfo:FormGroup;
  qualificationInfoList:any;
  requestPost = new RequestPost();
  loggedUserObj:any;
  editableUserObj:any;
  role:any;
  userObj:any;
  familyInfoData:any;
  parentsDetails:any;
  childrenDetails:any;
  staticMsgs:any;
  updateFlag:boolean = false;

  selectedDocumentString:any;
	
  constructor(
    private _fb : FormBuilder,
    private renderer2: Renderer2,
    private appDataService: AppDataService,
    private dataListingService: DataListingService,
    private sharedService: SharedService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
  this.prepareForm();
  this.editableUserObj = this.sharedService.editableFlag.value;
  this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
  if(this.editableUserObj != false && this.editableUserObj !=undefined){
    this.userObj = {"txt_Branch":this.editableUserObj.EU_BRANCH, "txt_UserID": this.editableUserObj.EU_CODE};
  }else{
    this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
  }
  this.role = this.loggedUserObj.ROLE;
  let menuQualification = {"txt_Menu_Type":"QUALI"};
  this.qualificaionInformationData(this.userObj, menuQualification);
  
  this.staticMsgs = this.sharedService.getStaticMessages();
  }
  
  prepareForm(){
	this.qualificationInfo = this._fb.group({
		QUA_YR_FR: [''],
		QUA_YR_TO: [''],
		COURSE: [''],
		SUBJECT: [''],
		INST_NAME: [''],
		INST_ADD1: [''],
		UNIVERSITY: [''],
		MARKS: [''],
		RANK: [''],
		Q_TYPE: [''],
		MAX_QUALI: [''],
		DOC: [''],
		DOC_CAT: ['']
	});
  }


  patchDataForm(qualifiData, updateFlag){
    this.qualificationInfo.patchValue({
      QUA_YR_FR: (qualifiData.QUA_YR_FR) ? moment(qualifiData.QUA_YR_FR, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      QUA_YR_TO: (qualifiData.QUA_YR_TO) ? moment(qualifiData.QUA_YR_TO, 'YYYY-MM-DD').format('DD/MM/YYYY') : '',
      COURSE: (qualifiData.COURSE) ? qualifiData.COURSE : '',
      SUBJECT: (qualifiData.SUBJECT) ? qualifiData.SUBJECT : '',
      INST_NAME: (qualifiData.INST_NAME) ? qualifiData.INST_NAME : '',
      INST_ADD1: (qualifiData.INST_ADD1) ? qualifiData.INST_ADD1 : '',
      UNIVERSITY: (qualifiData.UNIVERSITY) ? qualifiData.UNIVERSITY : '',
      MARKS: (qualifiData.MARKS) ? qualifiData.MARKS : '',
      RANK: (qualifiData.RANK) ? qualifiData.RANK : '',
      Q_TYPE: (qualifiData.Q_TYPE_C) ? qualifiData.Q_TYPE_C : '',
      MAX_QUALI: (qualifiData.MAX_QUALI) ? qualifiData.MAX_QUALI : '',
      DOC: (qualifiData.DOC) ? qualifiData.DOC : '',
      DOC_CAT: (qualifiData.DOC_CAT) ? qualifiData.DOC_CAT : ''
    });
    this.updateFlag = updateFlag;
  }

  
  qualificaionInformationData(userObj, menuQuali){
    const qualifInfoObj = Object.assign(userObj, menuQuali);
    this.appDataService.getQualificationListDetails(qualifInfoObj).subscribe(response=>{
      this.qualificationInfoList = response;
    });

    this.requestPost.ECODE = this.userObj.txt_UserID;
    this.requestPost.CreateBy = this.loggedUserObj.E_CODE;
    this.requestPost.UpdateBy = this.loggedUserObj.E_CODE;
    this.requestPost.Branch = this.userObj.txt_Branch;
    this.requestPost.RecordType = (this.updateFlag)?'U':'I';
  }

  addnewQualification(){
    this.patchDataForm('', false);
  }

  eidtQualification(SRL){
    this.renderer2.addClass(document.querySelector('#Qualif_Div'), "show");
    if(this.qualificationInfoList.length>0){
      this.qualificationInfoList.forEach(element => {
        if(element.SRL === SRL){
          this.patchDataForm(element, true);
        }
      });
    }
  }

  onFileChanges(event) {
    let me = this;
    let file = event.target.files[0];
    let fileName = event.target.files[1];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event:any) => {
      this.selectedDocumentString = event.target.result;
      this.qualificationInfo.patchValue({
        DOC: this.selectedDocumentString,
        DOC_CAT: fileName
      });
    }
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }
  
  saveQualificationInformation(){
	this.spinner.show();
    var formData:any = this.qualificationInfo.getRawValue();
    console.log("this.selectedDocumentString tada-->",this.selectedDocumentString);
    var qualificationDataObj:any = Object.assign(formData, this.requestPost);
    this.appDataService.saveQualificationInformation(qualificationDataObj).subscribe(response=>{
      if(response.Code=="True"){
        this.patchDataForm('', false);
        this.qualificaionInformationData(this.userObj, {"txt_Menu_Type":"QUALI"});
        //this.showSuccess(this.staticMsgs["success_messages"].sucess_qualification_info);
        this.showSuccess(response.MSG);
      }else{
        //this.showError(this.staticMsgs["error_messages"].error_qualification_info);
        this.showError(response.MSG);
      }
      this.spinner.hide();
    },err=>{
      this.spinner.hide();
      //this.showError(this.staticMsgs["error_messages"].error_qualification_info);
      this.showError("Web Service Error... 409");
    });
  }
  
  /** Toast Message Starts **/
  showSuccess(msg) {
    this.messageService.add({severity:'success', summary: 'Success Message', detail:msg});
  }
  showError(msg) {
      this.messageService.add({severity:'error', summary: 'Error Message', detail:msg});
  }
  /** Toast Message Ends **/

}
