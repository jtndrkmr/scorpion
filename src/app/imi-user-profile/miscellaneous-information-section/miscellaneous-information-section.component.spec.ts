import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiscellaneousInformationSectionComponent } from './miscellaneous-information-section.component';

describe('MiscellaneousInformationSectionComponent', () => {
  let component: MiscellaneousInformationSectionComponent;
  let fixture: ComponentFixture<MiscellaneousInformationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiscellaneousInformationSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiscellaneousInformationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
