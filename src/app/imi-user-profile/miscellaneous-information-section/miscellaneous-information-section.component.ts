import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppDataService } from './../../_services/user-data-service';
import { DataListingService } from './../../_services/data-listing-service';
import { SharedService } from './../../_services/shared-service';
import { RequestPost } from './../../models/imi-request-post-model';
import * as moment from 'moment';
import {MessageService} from 'primeng/api';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'imi-miscellaneous-information-section',
  templateUrl: './miscellaneous-information-section.component.html',
  styleUrls: ['./miscellaneous-information-section.component.css'],
  providers: [MessageService]
})
export class MiscellaneousInformationSectionComponent implements OnInit {

  miscellaneousInformationForm:FormGroup;
  requestPost = new RequestPost();
  hrEditable:any;
  eCodeVal:any;
  public viewFieldDetailArr:any;
  public viewFields:any;
  public requiredFields:any;
  misleanInfo:any;
  loggedUserObj:any;
  editableUserObj:any;
  role:any;
  userObj:any;
  staticMsgs:any;
  citylist:any= []; 
  banklist:any=[];
  

  constructor(
    private _fb : FormBuilder,
    private appDataService: AppDataService,
    private dataListingService: DataListingService,
    private sharedService: SharedService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.prepareForm();

    this.editableUserObj = this.sharedService.editableFlag.value;
    var fromlistingPage = this.sharedService.editableFlag.value;
    this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
    console.log(this.editableUserObj);
    if(this.editableUserObj != false && this.editableUserObj !=undefined && fromlistingPage){
      this.hrEditable = true;
      this.eCodeVal = this.editableUserObj.EU_CODE
      this.userObj = {"txt_Branch":this.editableUserObj.EU_BRANCH, "txt_UserID": this.editableUserObj.EU_CODE};
    }else{
      this.hrEditable = false;
      this.eCodeVal = this.loggedUserObj.E_CODE;
      this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
    }
    this.StaticListRecord(this.loggedUserObj.Branch);

    this.role = this.loggedUserObj.ROLE;
    let menuFamily = {"txt_Menu_Type":"MISCELUS"};

    this.miscellaneousInformation(this.userObj, menuFamily, this.hrEditable);
    this.staticMsgs = this.sharedService.getStaticMessages();
    
  }

  prepareForm(){
    this.miscellaneousInformationForm = this._fb.group({
      P_F_NO: [''],
      PEN_NO: [''],
      ESI_NO: [''],
      E_FILE_NO: [''],
      UAN: [''],
      PT_Location: [''],
      //CITY_CD: [''],
      IFSC: [''],
      BANK_CODE: [''],
      BANKACC: [''],
      //IFSC: [''],
      Reimb_Bank_Code: [''],
      Reimb_BankAcc: ['']
    })
  }

  StaticListRecord(branch){
    var reqObj:any = {"txt_Branch":branch, "txt_Type":"GI_ALL"};
    this.dataListingService.getStaticValuesList(reqObj).subscribe(response=>{
      this.citylist = response.GI_JU; 
      this.banklist = response.GI_BANK; 
    });
    
  }

  miscellaneousInformation(userObj, menuCnt, hrEditable){
    const miscellanInfoObj = Object.assign(userObj, menuCnt);
    this.appDataService.getMiscellaneousInformationDetails(miscellanInfoObj).subscribe(response=>{
      this.misleanInfo = response;
      this.patchMiscellaneousData(this.misleanInfo);
      this.viewFieldDetailArr = this.filterFieldsByRole(this.misleanInfo, this.loggedUserObj.ROLE, hrEditable);
      this.viewFields = this.filterViewFields(this.viewFieldDetailArr);
      this.requiredFields = this.filterRequiredFields(this.viewFieldDetailArr, this.loggedUserObj.ROLE, hrEditable);
    });

    this.requestPost.ECODE = this.userObj.txt_UserID;
    this.requestPost.CreateBy = this.loggedUserObj.E_CODE;
    this.requestPost.UpdateBy = this.loggedUserObj.E_CODE;
    this.requestPost.Branch = this.userObj.txt_Branch;
    this.requestPost.RecordType = 'U';
  }

  patchMiscellaneousData(miscelleousData){
    //console.log("Tada Miscellaneous -->",miscelleousData);
    this.miscellaneousInformationForm.patchValue({
      P_F_NO: miscelleousData.P_F_NO.VAL,
      PEN_NO: miscelleousData.PEN_NO.VAL,
      ESI_NO: miscelleousData.ESI_NO.VAL,
      E_FILE_NO: miscelleousData.E_FILE_NO.VAL,
      UAN: miscelleousData.UAN.VAL,
      PT_Location: miscelleousData.PT_Location.VAL,
      //CITY_CD: miscelleousData.CITY_CD.VAL,
      IFSC: miscelleousData.IFSC.VAL,
      BANK_CODE: miscelleousData.BANK_CODE.VAL,
      BANKACC: miscelleousData.BANKACC.VAL,
      Reimb_Bank_Code: miscelleousData.Reimb_Bank_Code.VAL,
      Reimb_BankAcc: miscelleousData.Reimb_BankAcc.VAL      
    });
  }

  filterFieldsByRole(generalData:any, role:string, hrEditable:boolean){
    var fieldsArray:any = [];
    if(hrEditable == true){
      for (var key in generalData) {
        if(generalData[key].HV=='Y') { fieldsArray.push(generalData[key]); generalData[key]["field"]=key; }
      }
    }else{
      for (var key in generalData) {
        if(generalData[key].UV=='Y') { fieldsArray.push(generalData[key]); generalData[key]["field"]=key; }
      }
    }
    return fieldsArray;
  }

  filterViewFields(viewFieldDetailArr:any){
    let fieldsArray:any = [];
    viewFieldDetailArr.forEach(element => {
      fieldsArray.push(element.field);
    });
    return fieldsArray;
  }



  filterRequiredFields(viewFieldDetailArr:any, role:any, hrEditable:boolean){

    let requiredField:any = [];
    var keys:any = this.miscellaneousInformationForm.getRawValue();
    
    if(hrEditable==true){
      viewFieldDetailArr.forEach(element => {
        const control = this.miscellaneousInformationForm.get(element.field);
        if(control!=null)
        { 
          if(element.HE=='Y') {  
              if(element.RO=='Y'){
              requiredField.push(element.field);                      
              this.miscellaneousInformationForm.controls[element.field].setValidators([Validators.required]);
              }              
          }
          else
          {
            this.miscellaneousInformationForm.controls[element.field].disable();             
          }
        }         
      });
    }
    else
    {
      
      viewFieldDetailArr.forEach(element => {
        const control = this.miscellaneousInformationForm.get(element.field);
        if(control!=null)
        {
          if(element.UE=='Y') {  
              if(element.RO=='Y'){
              requiredField.push(element.field);                      
              this.miscellaneousInformationForm.controls[element.field].setValidators([Validators.required]);              
              }             
          }
          else
          {
            this.miscellaneousInformationForm.controls[element.field].disable();              
          }
        }         
      });
    }

    requiredField.push("UAN");  
    this.miscellaneousInformationForm.controls["UAN"].setValidators([Validators.required,Validators.minLength(12)]);
    return requiredField;
  }

  /*filterRequiredFields(viewFieldDetailArr:any, role:any, hrEditable:boolean){
    let requiredField:any = [];
    var keys:any = this.miscellaneousInformationForm.getRawValue();

    if(hrEditable==true){
      viewFieldDetailArr.forEach(element => {
        if(element.HE=='Y') { 
          requiredField.push(element.field);
          for(let key in keys){
            if(key == element.field) 
              this.miscellaneousInformationForm.controls[element.field].setValidators([Validators.required]);
          }
        }else{
          for(let key in keys){
            if(key == element.field)
            this.miscellaneousInformationForm.controls[element.field].disable();
          }
        }
      });
    }else{
      viewFieldDetailArr.forEach(element => {
        if(element.UE=='Y') { 
          requiredField.push(element.field); 
          for(let key in keys){
            if(key == element.field) 
              this.miscellaneousInformationForm.controls[element.field].setValidators([Validators.required]);
          }
        }else{
          //console.log("element.field tada-->",element.field);
          for(let key in keys){
            if(key == element.field)
            this.miscellaneousInformationForm.controls[element.field].disable();
          }
          
        }
      });
    }
    return requiredField;
  }
  */
 isFieldValid(field: string,form) {    
  return this.miscellaneousInformationForm.get(field).enabled && this.miscellaneousInformationForm.get(field).invalid && (this.miscellaneousInformationForm.get(field).dirty || this.miscellaneousInformationForm.get(field).touched) ;
}

 validateAllFormFields(){   
  var fieldsControls = this.miscellaneousInformationForm.controls;     
  for (let field in this.requiredFields) {
    //console.log(this.requiredFields);     
    const control = this.miscellaneousInformationForm.get(this.requiredFields[field]);
    if(control!=null)
    {            
   control.markAsTouched({ onlySelf: true });
    }
  }
}




  saveMiscellaneousInformation(){
    this.spinner.show();
    this.validateAllFormFields();
    if(this.miscellaneousInformationForm.status == "VALID"){
    var formData:any = this.miscellaneousInformationForm.getRawValue();
    var contactFormDataObj:any = Object.assign(formData, this.requestPost);

    this.appDataService.saveMiscellaneousInformation(contactFormDataObj).subscribe(response=>{
      if(response.Code=="True"){
        this.showSuccess(response.MSG);
        //this.showSuccess(this.staticMsgs["success_messages"].sucess_miscellaneous_info);
      }else{
        this.showError(response.MSG);
        //this.showError(this.staticMsgs["error_messages"].error_miscellaneous_info);
      }
      this.spinner.hide();
    },err=>{      
      this.showError("Web Service Error... 409");
      this.spinner.hide();
      //this.showError(this.staticMsgs["error_messages"].error_miscellaneous_info);
    });
  }
  else
  {
    this.showError("Form Validation Failed....");
    this.spinner.hide();
  }
  }

  /** Toast Message Starts **/
  showSuccess(msg) {
    
    this.messageService.add({severity:'success', summary: 'Success Message', detail:msg});
  }
  showError(msg) {
    
      this.messageService.add({severity:'error', summary: 'Error Message', detail:msg});
  }
  /** Toast Message Ends **/

}
