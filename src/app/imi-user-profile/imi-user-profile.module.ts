import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImiUserProfileRoute } from './imi-user-profile.routes';
import { HeaderModule } from '../header/header.module';

import { TableModule } from 'primeng/table';
import {DropdownModule} from 'primeng/primeng';
import { DataTableModule } from 'primeng/primeng';
import {MultiSelectModule} from 'primeng/primeng';
import {ChartModule} from 'primeng/chart';
import {ToastModule} from 'primeng/toast';
import {CalendarModule} from 'primeng/calendar';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
//import { MessagesModule, MessageModule, DialogModule, ConfirmDialogModule, FileUploadModule, ProgressBarModule, OverlayPanelModule, TooltipModule } from 'primeng/primeng'; 

import { UserProfileSectionComponent } from './user-profile-section/user-profile-section.component';
import { DashboardSectionComponent } from './dashboard-section/dashboard-section.component';
import { GeneralInformationSectionComponent } from './general-information-section/general-information-section.component';
import { UsersListingSectionComponent } from './users-listing-section/users-listing-section.component';
import { FamilyInformationSectionComponent } from './family-information-section/family-information-section.component';
import { ContactInformationSectionComponent } from './contact-information-section/contact-information-section.component';
import { QualificationInformationSectionComponent } from './qualification-information-section/qualification-information-section.component';
import { WorkExperienceInformationSectionComponent } from './work-experience-information-section/work-experience-information-section.component';
import { TrainingInformationSectionComponent } from './training-information-section/training-information-section.component';
import { MiscellaneousInformationSectionComponent } from './miscellaneous-information-section/miscellaneous-information-section.component';
import { IdDetailsInformationSectionComponent } from './id-details-information-section/id-details-information-section.component';
import { SearchableListingComponent } from './searchable-listing/searchable-listing.component';
import { SalaryListingSectionComponent } from './salary-listing-section/salary-listing-section.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    ImiUserProfileRoute,
    HeaderModule,
    TableModule,
    DropdownModule,
    DataTableModule,
    MultiSelectModule,
    ChartModule,
    ToastModule,
    CalendarModule,
    ProgressSpinnerModule
  ],
  declarations: [
    UserProfileSectionComponent, 
    DashboardSectionComponent, 
    GeneralInformationSectionComponent,
    UsersListingSectionComponent,
    FamilyInformationSectionComponent,
    ContactInformationSectionComponent,
    QualificationInformationSectionComponent,
    WorkExperienceInformationSectionComponent,
    TrainingInformationSectionComponent,
    MiscellaneousInformationSectionComponent,
    IdDetailsInformationSectionComponent,
    SearchableListingComponent,
    SalaryListingSectionComponent,
  ],
  
})
export class ImiUserProfileModule { }
