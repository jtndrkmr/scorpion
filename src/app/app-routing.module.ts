import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards/auth.guard';

const routes: Routes = [
  {path:'', pathMatch: 'full', redirectTo:'login'},
  { path: 'login', loadChildren:'./login/login.module#LoginModule'},
  { path: 'dashboard', loadChildren:'./imi-user-profile/imi-user-profile.module#ImiUserProfileModule', canActivate: [AuthGuard]},
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  {path:'**', redirectTo:''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
