import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from "rxjs";
import { EnvironmentConfigurationModel } from '../models/environment-configuration-model';

@Injectable()

export class DataListingService{
    
    domainServiceURL:any;
    
    constructor(
        private http: HttpClient,
        private environmentConfigurationModel: EnvironmentConfigurationModel
    ){ 
        this.domainServiceURL = this.environmentConfigurationModel.getDomainServiceURL();
    }

    getDeginationList(paramData):any{
        let serviceURL = this.domainServiceURL+"List_Of_Values.asmx/List_Of_Values_s";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getStaticValuesList(paramData):any{
        let serviceURL = this.domainServiceURL+"List_Of_Values.asmx/List_Of_Values_s";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }



    /** JSON Parameter & Option Header Config Start*/
    prepareJsonData(paramData):any{
        return JSON.stringify(paramData);
    }

    getOptions():any{
        let headers = new HttpHeaders({"Content-Type":"application/json; charset=UTF-8"});
        let options = { headers: headers };
        return options;
    }
    /** JSON Parameter & Option Header Config Ends*/
}