import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { EnvironmentConfigurationModel } from '../models/environment-configuration-model';

@Injectable()

export class AppDataService{

    domainServiceURL:any;

    constructor(
        private http: HttpClient,
        private environmentConfigurationModel: EnvironmentConfigurationModel
    ){ 
        this.domainServiceURL = this.environmentConfigurationModel.getDomainServiceURL();
    }

    authentication(paramData):any{
        let serviceURL = this.domainServiceURL+"Login.asmx/Login_main";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    sidebarMenuByUserRole(paramData):any{
        let serviceURL = this.domainServiceURL+"Menu_Master.asmx/menu_master";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    headerAccordianSearch(paramData):any{
        let serviceURL = this.domainServiceURL+"Menu_Master.asmx/menu_master_list";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getPersonalInformationDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"v_h_Profile.asmx/V_H_PROFILE";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getUserBasicDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"Basic_Details.asmx/Basic_Details_s";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getLatestECODEValue(paramData):any{
        let serviceURL = this.domainServiceURL+"lst_emp_code.asmx/lst_emp_code_s";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getUsersListingByRole(paramData):any{
        let serviceURL = this.domainServiceURL+"PersonalMasterMain_data.asmx/PersonalMasterMain_data_s";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getGeneralInformationDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"h_Personal_Master_Controller_details.asmx/field_master";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getFamilyInformationDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"Family_Info_Master.asmx/Family_Info_Master_s";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getContactInformationDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"h_Personal_Master_Controller_details.asmx/field_master";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getQualificationListDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"Qualification_Info_Master.asmx/Qualification_Info_Master_s";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getWorkExperienceListDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"Work_Exp_Master.asmx/get_data";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    geTrainingListDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"Training_Info_Master.asmx/GET_DATA";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    geIdDetailsListDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"ID_Info_Master.asmx/GET_DATA";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    getMiscellaneousInformationDetails(paramData):any{
        let serviceURL = this.domainServiceURL+"h_Personal_Master_Controller_details.asmx/field_master";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }




















    staticMessagesData():any{
        return this.http.get('./assets/toastertxts/imi-toststxt.json');
    }

    saveProfileImage(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/Upload_Profile_PIC_P.asmx/DATA_ENTRY";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    saveGeneralInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/h_Personal_Master_GEN_P.asmx/GEN_ENTRY";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    saveFamilyParentsInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/Family_Info_Master_P.asmx/FAM_ENTRY";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    saveFamilyChildrenInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/Family_Info_Master_P.asmx/FAM_CHILD_ENTRY";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }
    delFamilyChildrenInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/Family_Info_Master_P.asmx/FAM_CHILD_DELETE";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    saveContactInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/Contact_Info_Master_P.asmx/CONTACT_ENTRY";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    saveQualificationInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/Qualification_Info_Master_P.asmx/QUALI_ENTRY";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    saveWorkExperienceInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/Work_Exp_Master_P.asmx/WORK_ENTRY";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    saveTrainingInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/Training_Info_Master_P.asmx/ENTRY_DATA";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    saveIdDetailInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/ID_Info_Master_P.asmx/ENTRY_DATA";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }

    saveMiscellaneousInformation(paramData):any{
        let serviceURL = this.domainServiceURL+"POST/Misc_Info_Master_P.asmx/DATA_ENTRY";
        return this.http.post(serviceURL, paramData, this.getOptions());
    }














    

    /** JSON Parameter & Option Header Config Start*/
    prepareJsonData(paramData):any{
        return JSON.stringify(paramData);
    }

    getOptions():any{
        let headers = new HttpHeaders({"Content-Type":"application/json; charset=UTF-8"});
        let options = { headers: headers };
        return options;
    }
    /** JSON Parameter & Option Header Config Ends*/
}