import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn : 'root'
})

export class SharedService{
    private loggedUserObject = new BehaviorSubject<any>(false);
    private sidebarObject = new BehaviorSubject<any>(false);
    public editableFlag = new BehaviorSubject<any>(false);
    public salaryeditableFlag = new BehaviorSubject<any>(false);
    public msgstxt = new BehaviorSubject<any>(false);
    
    setLoggedUserObject(data:any){
        this.loggedUserObject.next(data);
    }
    getLoggedUserObject():any{
        return this.loggedUserObject.value;
    }

    setSidebarMenuObject(data:any){
        this.sidebarObject.next(data);
    }
    getSidebarMenuObject(){
        return this.sidebarObject.value;
    }

    setEditableFormFlag(data:any){
        this.editableFlag.next(data);
    }

    setSalaryMasterFormFlag(data:any){
        this.salaryeditableFlag.next(data);
    }

    setStaticMessages(data:any){
        this.msgstxt.next(data);
    }
    getStaticMessages(){
        return this.msgstxt.value;
    }
    
}