import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EnvironmentConfigurationModel } from '../models/environment-configuration-model';
//import { User } from '@/_models';

@Injectable({ providedIn: 'root' })

export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    private domainServiceURL:any;

    constructor(
        private http: HttpClient,
        private environmentConfigurationModel: EnvironmentConfigurationModel
    ){
        this.domainServiceURL = this.environmentConfigurationModel.getDomainServiceURL();
        this.currentUserSubject = new BehaviorSubject<any>(localStorage.getItem('currentUser'));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue():any {
        return this.currentUserSubject.value;
    }

    login(paramData):any {
        let serviceURL = this.domainServiceURL+"Login.asmx/Login_main";
        return this.http.post(serviceURL, paramData, this.getOptions());
        /*return this.http.post<any>('this.domainServiceURL+"Login.asmx/Login_main"', paramData);
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }
                localStorage.setItem('currentUser', JSON.stringify(user));
                return user;
            }));*/
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    /** JSON Parameter & Option Header Config Start*/
    prepareJsonData(paramData):any{
        return JSON.stringify(paramData);
    }

    getOptions():any{
        let headers = new HttpHeaders({"Content-Type":"application/json; charset=UTF-8"});
        let options = { headers: headers };
        return options;
    }
    /** JSON Parameter & Option Header Config Ends*/
}