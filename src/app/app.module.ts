import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppDataService } from './_services/user-data-service';
import { DataListingService } from './_services/data-listing-service';
import { SharedService } from './_services/shared-service';
import { EnvironmentConfigurationModel } from './models/environment-configuration-model';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MessageService} from 'primeng/api';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxSpinnerModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    AppDataService,
    DataListingService,
    SharedService,
    EnvironmentConfigurationModel,
    MessageService
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule {
  constructor(private appDataService:AppDataService, private sharedService: SharedService){
    this.appDataService.staticMessagesData().subscribe(response=>{
      this.sharedService.setStaticMessages(response);
    });
  }
 }
