import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSidebarSectionComponent } from './user-sidebar-section.component';

describe('UserSidebarSectionComponent', () => {
  let component: UserSidebarSectionComponent;
  let fixture: ComponentFixture<UserSidebarSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSidebarSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSidebarSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
