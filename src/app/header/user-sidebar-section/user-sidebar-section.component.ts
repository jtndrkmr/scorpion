
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppDataService } from './../../_services/user-data-service';
import { SharedService } from './../../_services/shared-service';

@Component({
  selector: 'imi-user-sidebar-section',
  templateUrl: './user-sidebar-section.component.html',
  styleUrls: ['./user-sidebar-section.component.css']
})

export class UserSidebarSectionComponent implements OnInit {

  public loggedUserObj:any;
  public userObj:any;
  menuData:any;
  profileImage:any;

  constructor(
    private appDataService: AppDataService,
    private sharedService : SharedService
  ) { }

  ngOnInit() {
    this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
    this.convertingImage(this.loggedUserObj.User_Profile_Image, this.loggedUserObj.IMAGE_TYPE);
    this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};
    this.sideBarMenuByUser(this.userObj);
    this.staticMessagesFunction();
  }

  convertingImage(blobImg,IMAGE_TYPE){
    let objectURL = 'data:image/'+IMAGE_TYPE+';base64,'+blobImg;
    this.profileImage = objectURL;
  }

  sideBarMenuByUser(userObject){
    this.appDataService.sidebarMenuByUserRole(userObject).subscribe(response=>{
      this.menuData = response;
      this.sharedService.setSidebarMenuObject(this.menuData);
      //console.log("Menu Response-->",response);
    });
  }

  staticMessagesFunction(){
    
  }

  /*filterMenuList(menuObj){
    menuObj.forEach(element => {
      
      if(element.Child_Menu != []){
        console.log("Child Menu-->",element.Child_Menu);
        let childListArr = '<li class="nav-item"> <a href="../../index.html" class="nav-link"> <i class="far fa-circle nav-icon"></i> <p>Dashboard v1</p> </a> </li>';
      }
    });
  }*/

}
