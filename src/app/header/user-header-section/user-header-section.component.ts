import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from './../../_services/authentication.service';
import { SharedService } from './../../_services/shared-service';
import { AppDataService } from './../../_services/user-data-service';

@Component({
  selector: 'imi-user-header-section',
  templateUrl: './user-header-section.component.html',
  styleUrls: ['./user-header-section.component.css']
})
export class UserHeaderSectionComponent implements OnInit {

  searchbarList:any;
  searchbarObj:any;
  selectedCar: string;
  loggedUserObj:any;
  userObj:any;
  
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private sharedService: SharedService,
    private appDataService: AppDataService
  ) { }

  ngOnInit() {
    this.loggedUserObj = JSON.parse(this.sharedService.getLoggedUserObject());
    this.userObj = {"txt_Branch":this.loggedUserObj.Branch, "txt_UserID": this.loggedUserObj.E_CODE};

    this.creatingSearchList(this.userObj);
  }

  creatingSearchList(searchbarObj):any{
    this.appDataService.headerAccordianSearch(searchbarObj).subscribe(response=>{
      this.searchbarList = response;
    });
  }

  logout(){
    this.authenticationService.logout();
    this.router.navigate(['login']);
    setTimeout(()=>{
      window.location.reload();
    },1000);
    
  }

}
