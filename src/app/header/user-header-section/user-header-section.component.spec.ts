import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserHeaderSectionComponent } from './user-header-section.component';

describe('UserHeaderSectionComponent', () => {
  let component: UserHeaderSectionComponent;
  let fixture: ComponentFixture<UserHeaderSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserHeaderSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserHeaderSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
