import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { RouterModule} from '@angular/router';
import { AdminHeaderSectionComponent } from './admin-header-section/admin-header-section.component';
import { UserHeaderSectionComponent } from './user-header-section/user-header-section.component';
import { UserFooterSectionComponent } from './user-footer-section/user-footer-section.component';
import { UserSidebarSectionComponent } from './user-sidebar-section/user-sidebar-section.component';
import { TableModule } from 'primeng/table';
import {DropdownModule} from 'primeng/primeng';
import { DataTableModule } from 'primeng/primeng';
import {MultiSelectModule} from 'primeng/primeng';

@NgModule({
  declarations: [
    AdminHeaderSectionComponent, 
    UserHeaderSectionComponent, UserFooterSectionComponent, UserSidebarSectionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    TableModule,
    DropdownModule,
    DataTableModule,
    MultiSelectModule
  ],
  providers:[
    
  ],
  exports:[
    UserHeaderSectionComponent,
    UserSidebarSectionComponent
  ]
})
export class HeaderModule { }
