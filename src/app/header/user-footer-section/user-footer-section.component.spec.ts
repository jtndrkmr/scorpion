import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFooterSectionComponent } from './user-footer-section.component';

describe('UserFooterSectionComponent', () => {
  let component: UserFooterSectionComponent;
  let fixture: ComponentFixture<UserFooterSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFooterSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFooterSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
